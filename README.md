# Asp.Net-Core-Inventory-Order-Management-System
Project example Asp.Net Core Mvc implementation of inventory order management system. warehouse, box, pallet, vendor, customer and more.


# Features

- Asp.Net Core Mvc
- EF / Entity Framework
- Code First
- C#
- Beautiful Bootstrap
- Web API + Jquery + Angular
- SendGrid
- Upload Profile Picture
- Edit Profile
- Change Password
- Users, Roles and Memberships

# Functional Features

- Dashboard / Chart Example
- Customers  
  - Customer
  - Agent
  - Vendor
- Invoices
  - Invoice
  - Bill
- Inventory
  - Box
  - Pallet
- User & Role
  - User
  - Change Password
  - Role
  - Change Role


# Development Tools & Environment

- **Visual Studio Code
- **Syncfusion JQuery Controls EJ1 (Community Edition)**. (https://www.syncfusion.com/products/communitylicense)

- **you can find the original code from 

- https://github.com/go2ismail/Asp.Net-Core-Inventory-Order-Management-System/tree/master/coderush