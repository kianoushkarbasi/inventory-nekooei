(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/edit/edit.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/edit/edit.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-3\">\n        <div class=\"row ml-1 mr-1 mt-3 mb-5 text-light \">\n          <button\n            class=\"btn bg-secondary  btn-block col-md-3\"\n            [routerLink]=\"['../home']\"\n          >\n            back\n          </button>\n        </div>\n        <form>        \n          <div class=\"mb-3\">\n            <label for=\"agent.Name\">Agent name:</label>\n            <input\n              class=\"form-control\"\n              id=\"agent.Name\"\n              name=\"agent.Namer\"\n              type=\"text\"\n              [(ngModel)]=\"agent.Name\"\n            />\n          </div>\n          <div class=\"mb-3\">\n            <label for=\"agent.Address\">Address:</label>\n            <input\n              class=\"form-control\"\n              id=\"agent.Address\"\n              name=\"agent.Address\"\n              type=\"text\"\n              [(ngModel)]=\"agent.Address\"\n            />\n          </div>\n          <div class=\"mb-3\">\n            <label for=\"agent.Address\">City:</label>\n            <input\n              class=\"form-control\"\n              id=\"agent.City\"\n              name=\"agent.City\"\n              type=\"text\"\n              [(ngModel)]=\"agent.City\"\n            />\n          </div>\n          <div class=\"mb-3\">\n            <label for=\"agent.ZipCode\">ZipCode:</label>\n            <input\n              class=\"form-control\"\n              id=\"agent.ZipCode\"\n              name=\"agent.ZipCode\"\n              type=\"text\"\n              [(ngModel)]=\"agent.ZipCode\"\n            />\n          </div>\n          <div class=\"mb-3\">\n            <label for=\"agent.Phone\">Phone:</label>\n            <input\n              class=\"form-control\"\n              id=\"agent.Phone\"\n              name=\"agent.Phone\"\n              type=\"text\"\n              [(ngModel)]=\"agent.Phone\"\n            />\n          </div>\n          <div class=\"mb-3\">\n            <label for=\"agent.MobilePhone\">Mobile Phone:</label>\n            <input\n              class=\"form-control\"\n              id=\"agent.MobilePhone\"\n              name=\"agent.MobilePhone\"\n              type=\"text\"\n              [(ngModel)]=\"agent.MobilePhone\"\n            />\n          </div>\n          <div class=\"mb-3\">\n            <label for=\"agent.Email\">Email:</label>\n            <input\n              class=\"form-control\"\n              id=\"agent.Email\"\n              name=\"agent.Email\"\n              type=\"text\"\n              [(ngModel)]=\"agent.Email\"\n            />\n          </div>\n          <div class=\"mb-3\">\n            <label for=\"agent.Website\">Website:</label>\n            <input\n              class=\"form-control\"\n              id=\"agent.Website\"\n              name=\"agent.Website\"\n              type=\"text\"\n              [(ngModel)]=\"agent.Website\"\n            />\n          </div>\n          <div class=\"mb-3\">\n            <label for=\"agent.Vergi\">Vergi:</label>\n            <input\n              class=\"form-control\"\n              id=\"agent.Vergi\"\n              name=\"agent.Vergi\"\n              type=\"text\"\n              [(ngModel)]=\"agent.Vergi\"\n            />\n          </div>\n          <hr/>\n          <label>Agent Customers:</label>\n          <mat-form-field class=\"col\">\n                <input\n                  type=\"text\"\n                  class=\"col ml-1\"\n                  name=\"newAgentCustomer\"\n                  placeholder=\"Customer\"\n                  [formControl]= \"CustomersControl\"\n                  matInput              \n                  [matAutocomplete]=\"auto\"          \n                />            \n                <mat-autocomplete #auto=\"matAutocomplete\">\n                  <mat-option\n                    *ngFor=\"let option of filteredOptions | async\"  \n                    (click) = \"UpdateCustomer(option)\"\n                    [value]=\"option.CustomerName\"            \n                  >\n                    {{ option.CustomerName }}\n                  </mat-option>\n                </mat-autocomplete>\n         </mat-form-field>  \n         <table class=\"table table-striped able-sm mt-2\">\n            <tr *ngFor=\"let customer of agent.Customers\">\n                <td>{{customer.CustomerId}}</td>\n                <td>{{customer.Customer.CustomerName}}</td>\n                <td>{{customer.Customer.Phone}}</td>\n                <td ><input class=\"btn btn-danger\" (click)=\"RemoveItem(customer)\" type=\"button\" value=\"X\"/></td>\n            </tr>\n         </table>\n         <p class=\"bg-success p-3 mb-2\" *ngIf=\"showSuccess\">Successfully Saved!</p>\n         <div class=\"row\">\n            <div class=\"col-md-3 mb-3\">\n              <button\n                class=\"btn btn-secondary btn-lg btn-block\"\n                type=\"button\"\n                (click)=\"Cancel()\"\n              >\n                Cancel\n              </button>\n            </div>\n            <div class=\"col-md-3 mb-3\">\n                    <button\n                      class=\"btn btn-danger btn-lg btn-block\"\n                      type=\"button\"\n                      (click)=\"Delete()\"\n                    >\n                      Delete\n                    </button>\n              </div>          \n          \n            <div class=\"col-md-3 mb-3\">\n              <button\n                class=\"btn btn-success btn-lg btn-block\"\n                type=\"button\"\n                value=\"Save\"\n                (click)=\"Save(agent)\"\n              >\n                Save\n              </button>\n            </div>\n          </div>\n        </form>\n      </div>\n      "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\" box-header with-border mt-3\">\n    <input\n      [(ngModel)]=\"searchText\"\n      placeholder=\"search text goes here\"\n      class=\"form-control col-md-3 ml-2 mb-3\"\n    />\n    <a\n      class=\"btn btn-success btn-md btn-block col-md-1 ml-2 mb-3\"\n      [routerLink]=\"['../insert']\"\n      >Add</a\n    >\n    <table class=\"table table-striped able-sm mt-2\">\n      <thead class=\"thead-dark\">\n        <tr>\n          <th class=\"d-none d-sm-block\" scope=\"col\">#</th>\n          <th scope=\"col\">Agent Name</th>\n          <th scope=\"col\">Address</th>\n          <th class=\"hidden-sm hidden-xs\" scope=\"col\">City</th>\n          <th class=\"hidden-sm hidden-xs\" scope=\"col\">Zip Code</th>\n          <th class=\"hidden-sm hidden-xs\" scope=\"col\">Phone</th>\n          <th scope=\"col\">Mobile Phone</th>\n          <th class=\"hidden-sm hidden-xs\" scope=\"col\">Email</th>\n          <th class=\"d-none d-sm-block\" scope=\"col\">Website</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr\n          *ngFor=\"let agent of agents | filter: searchText\"\n          (click)=\"singleClick($event)\"\n          (dblclick)=\"doubleClick($event)\"\n        >\n          <td class=\"d-none d-sm-block\" scope=\"col\">{{ agent.AgentId }}</td>\n          <td scope=\"col\">{{ agent.Name }}</td>\n          <td scope=\"col\">{{ agent.Address }}</td>\n          <td class=\"hidden-sm hidden-xs\" scope=\"col\">{{ agent.City }}</td>\n          <td class=\"hidden-sm hidden-xs\" scope=\"col\">{{ agent.ZipCode }}</td>\n          <td class=\"hidden-sm hidden-xs\" scope=\"col\">{{ agent.Phone }}</td>\n          <td scope=\"col\">{{ agent.MobilePhone }}</td>\n          <td class=\"hidden-sm hidden-xs\" scope=\"col\">{{ agent.Email }}</td>\n          <td class=\"d-none d-sm-block\" scope=\"col\">{{ agent.Website }}</td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n  "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/insert/insert.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/insert/insert.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-3\">\n    <div class=\"row ml-1 mr-1 mt-3 mb-5 text-light \">\n      <button\n        class=\"btn bg-secondary  btn-block col-md-3\"\n        [routerLink]=\"['../home']\"\n      >\n        back\n      </button>\n    </div>\n    <form>        \n      <div class=\"mb-3\">\n        <label for=\"agent.Name\">Agent name:</label>\n        <input\n          class=\"form-control\"\n          id=\"agent.Name\"\n          name=\"agent.Namer\"\n          type=\"text\"\n          [(ngModel)]=\"agent.Name\"\n        />\n      </div>\n      <div class=\"mb-3\">\n        <label for=\"agent.Address\">Address:</label>\n        <input\n          class=\"form-control\"\n          id=\"agent.Address\"\n          name=\"agent.Address\"\n          type=\"text\"\n          [(ngModel)]=\"agent.Address\"\n        />\n      </div>\n      <div class=\"mb-3\">\n        <label for=\"agent.Address\">City:</label>\n        <input\n          class=\"form-control\"\n          id=\"agent.City\"\n          name=\"agent.City\"\n          type=\"text\"\n          [(ngModel)]=\"agent.City\"\n        />\n      </div>\n      <div class=\"mb-3\">\n        <label for=\"agent.ZipCode\">ZipCode:</label>\n        <input\n          class=\"form-control\"\n          id=\"agent.ZipCode\"\n          name=\"agent.ZipCode\"\n          type=\"text\"\n          [(ngModel)]=\"agent.ZipCode\"\n        />\n      </div>\n      <div class=\"mb-3\">\n        <label for=\"agent.Phone\">Phone:</label>\n        <input\n          class=\"form-control\"\n          id=\"agent.Phone\"\n          name=\"agent.Phone\"\n          type=\"text\"\n          [(ngModel)]=\"agent.Phone\"\n        />\n      </div>\n      <div class=\"mb-3\">\n        <label for=\"agent.MobilePhone\">Mobile Phone:</label>\n        <input\n          class=\"form-control\"\n          id=\"agent.MobilePhone\"\n          name=\"agent.MobilePhone\"\n          type=\"text\"\n          [(ngModel)]=\"agent.MobilePhone\"\n        />\n      </div>\n      <div class=\"mb-3\">\n        <label for=\"agent.Email\">Email:</label>\n        <input\n          class=\"form-control\"\n          id=\"agent.Email\"\n          name=\"agent.Email\"\n          type=\"text\"\n          [(ngModel)]=\"agent.Email\"\n        />\n      </div>\n      <div class=\"mb-3\">\n        <label for=\"agent.Website\">Website:</label>\n        <input\n          class=\"form-control\"\n          id=\"agent.Website\"\n          name=\"agent.Website\"\n          type=\"text\"\n          [(ngModel)]=\"agent.Website\"\n        />\n      </div>\n      <div class=\"mb-3\">\n        <label for=\"agent.Vergi\">Vergi:</label>\n        <input\n          class=\"form-control\"\n          id=\"agent.Vergi\"\n          name=\"agent.Vergi\"\n          type=\"text\"\n          [(ngModel)]=\"agent.Vergi\"\n        />\n      </div>\n      <hr/>\n      <label>Agent Customers:</label>\n      <mat-form-field class=\"col\">\n            <input\n              type=\"text\"\n              class=\"col ml-1\"\n              name=\"newAgentCustomer\"\n              placeholder=\"Customer\"\n              [formControl]= \"CustomersControl\"\n              matInput              \n              [matAutocomplete]=\"auto\"          \n            />            \n            <mat-autocomplete #auto=\"matAutocomplete\">\n              <mat-option\n                *ngFor=\"let option of filteredOptions | async\"  \n                (click) = \"UpdateCustomer(option)\"\n                [value]=\"option.CustomerName\"            \n              >\n                {{ option.CustomerName }}\n              </mat-option>\n            </mat-autocomplete>\n     </mat-form-field>  \n     <table class=\"table table-striped able-sm mt-2\">\n        <tr *ngFor=\"let customer of agent.Customers\">\n            <td>{{customer.CustomerId}}</td>\n            <td>{{customer.CustomerName}}</td>\n            <td>{{customer.Phone}}</td>\n            <td ><input class=\"btn btn-danger\" (click)=\"RemoveItem(customer)\" type=\"button\" value=\"X\"/></td>\n        </tr>\n     </table>\n     <p class=\"bg-success p-3 mb-2\" *ngIf=\"showSuccess\">Successfully Saved!</p>\n     <div class=\"row\">\n        <div class=\"col-md-3 mb-3\">\n          <button\n            class=\"btn btn-secondary btn-lg btn-block\"\n            type=\"button\"\n            (click)=\"Cancel()\"\n          >\n            Cancel\n          </button>\n        </div>\n        <div class=\"col-md-3 mb-3\">\n          <button\n            class=\"btn btn-success btn-lg btn-block\"\n            type=\"button\"\n            value=\"Save\"\n            (click)=\"Save(agent)\"\n          >\n            Save\n          </button>\n        </div>\n      </div>\n    </form>\n  </div>\n  "

/***/ }),

/***/ "./src/app/Agent.ts":
/*!**************************!*\
  !*** ./src/app/Agent.ts ***!
  \**************************/
/*! exports provided: Agent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Agent", function() { return Agent; });
class Agent {
}


/***/ }),

/***/ "./src/app/app.component.sass":
/*!************************************!*\
  !*** ./src/app/app.component.sass ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'Agent';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.sass */ "./src/app/app.component.sass")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _filter_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./filter.pipe */ "./src/app/filter.pipe.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _insert_insert_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./insert/insert.component */ "./src/app/insert/insert.component.ts");
/* harmony import */ var _edit_edit_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./edit/edit.component */ "./src/app/edit/edit.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/esm2015/autocomplete.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm2015/datepicker.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm2015/form-field.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");

















const appRoutes = [
    { path: "home", component: _home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"] },
    { path: "edit", component: _edit_edit_component__WEBPACK_IMPORTED_MODULE_7__["EditComponent"] },
    { path: "insert", component: _insert_insert_component__WEBPACK_IMPORTED_MODULE_6__["InsertComponent"] },
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "**", component: _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"] }
];
let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _filter_pipe__WEBPACK_IMPORTED_MODULE_3__["FilterPipe"],
            _home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"],
            _insert_insert_component__WEBPACK_IMPORTED_MODULE_6__["InsertComponent"],
            _edit_edit_component__WEBPACK_IMPORTED_MODULE_7__["EditComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_10__["RouterModule"].forRoot(appRoutes),
            _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_14__["MatFormFieldModule"],
            _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_12__["MatAutocompleteModule"],
            _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__["MatDatepickerModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatNativeDateModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_8__["BrowserAnimationsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatInputModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_11__["ReactiveFormsModule"]
        ],
        exports: [
            _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_12__["MatAutocompleteModule"],
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_14__["MatFormFieldModule"],
            _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__["MatDatepickerModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatNativeDateModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatInputModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_11__["ReactiveFormsModule"]
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/edit/edit.component.sass":
/*!******************************************!*\
  !*** ./src/app/edit/edit.component.sass ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VkaXQvZWRpdC5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/edit/edit.component.ts":
/*!****************************************!*\
  !*** ./src/app/edit/edit.component.ts ***!
  \****************************************/
/*! exports provided: EditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditComponent", function() { return EditComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _Agent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Agent */ "./src/app/Agent.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");







let EditComponent = class EditComponent {
    constructor(http, route, router) {
        this.http = http;
        this.route = route;
        this.router = router;
        this.agent = new _Agent__WEBPACK_IMPORTED_MODULE_2__["Agent"]();
        this.showSuccess = false;
        this.showError = false;
        this.myControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.CustomersControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.customers = new Array();
        this.agent.Customers = new Array();
    }
    ngOnInit() {
        var id = this.route.snapshot.queryParamMap.get("id");
        var customerUrl = "/api/Customer/";
        var agentUrl = "/api/Agent/GetAgent/";
        this.http.get(customerUrl)
            .subscribe(p => {
            this.customers = p.Items;
            this.filteredOptions = this.CustomersControl.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(value => this._filter(value)));
        });
        this.http.get(agentUrl + id)
            .subscribe(p => {
            this.agent = p;
        });
    }
    Delete() {
        this.showSuccess = false;
        var agentUrl = "/api/Agent/Remove/" + this.agent.AgentId;
        this.http
            .delete(agentUrl)
            .toPromise()
            .then(res => {
            this.showSuccess = true;
            this.router.navigateByUrl("/home");
        });
    }
    Save(agent) {
        this.showSuccess = false;
        var billURL = "/api/Agent/Update";
        this.http
            .put(billURL, agent)
            .toPromise()
            .then(res => {
            this.showSuccess = true;
        });
    }
    UpdateCustomer(option) {
        this.agent.Customers.push(option);
    }
    Cancel() {
        this.router.navigateByUrl("/home");
    }
    RemoveItem(customer) {
        const index = this.agent.Customers.indexOf(customer);
        if (index !== -1) {
            this.agent.Customers.splice(index, 1);
        }
    }
    _filter(value) {
        const filterValue = value.toString().toLowerCase();
        return this.customers.filter(option => option.CustomerName.toLowerCase().includes(filterValue));
    }
};
EditComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
EditComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit',
        template: __webpack_require__(/*! raw-loader!./edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/edit/edit.component.html"),
        styles: [__webpack_require__(/*! ./edit.component.sass */ "./src/app/edit/edit.component.sass")]
    })
], EditComponent);



/***/ }),

/***/ "./src/app/filter.pipe.ts":
/*!********************************!*\
  !*** ./src/app/filter.pipe.ts ***!
  \********************************/
/*! exports provided: FilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterPipe", function() { return FilterPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FilterPipe = class FilterPipe {
    transform(items, searchText) {
        if (!items)
            return [];
        if (!searchText)
            return items;
        searchText = searchText.toString().toLowerCase();
        var results = this.combinations(searchText.split(' '));
        return items.filter(it => {
            var included = false;
            for (var i = 0; i < results.length; i++) {
                var rr = JSON.stringify(it)
                    .toLowerCase()
                    .match(results[i]);
                if (rr != null)
                    included = rr.index > 0;
                if (included)
                    return true;
            }
            return included;
        });
    }
    combinations(arg) {
        var results = new Array();
        for (var i = 0; i < arg.length; i++) {
            var text = arg[i];
            for (var j = 0; j < arg.length; j++) {
                if (arg[i] != arg[j]) {
                    text = text + ".*" + arg[j];
                }
            }
            results.push(text);
        }
        return results;
    }
};
FilterPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: "filter"
    })
], FilterPipe);



/***/ }),

/***/ "./src/app/home/home.component.sass":
/*!******************************************!*\
  !*** ./src/app/home/home.component.sass ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let HomeComponent = class HomeComponent {
    constructor(http, router) {
        this.http = http;
        this.router = router;
        this.agents = new Array();
        this.preventSingleClick = false;
    }
    ngOnInit() {
        var agentsURL = "/api/Agent/";
        this.http
            .get(agentsURL)
            .toPromise()
            .then(res => {
            this.agents = res["Items"];
        });
    }
    singleClick(event) {
        this.preventSingleClick = false;
        const delay = 200;
        this.timer = setTimeout(() => {
            if (!this.preventSingleClick) {
                //Navigate on single click
                for (var i = 0; i < event.path[2].childElementCount; i++) {
                    event.path[2].children[i].className = "";
                }
                event.path[1].className = "bg-warning";
            }
        }, delay);
        var filterPallets = this.agents.filter(p => {
            return p.Name.toLowerCase().includes(this.searchText);
        });
    }
    doubleClick(event) {
        this.preventSingleClick = true;
        clearTimeout(this.timer);
        //Navigate on double click
        this.router.navigate(["/edit/"], {
            queryParams: { id: event.currentTarget.children[0].innerText }
        });
    }
};
HomeComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-home",
        template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html"),
        styles: [__webpack_require__(/*! ./home.component.sass */ "./src/app/home/home.component.sass")]
    })
], HomeComponent);



/***/ }),

/***/ "./src/app/insert/insert.component.sass":
/*!**********************************************!*\
  !*** ./src/app/insert/insert.component.sass ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2luc2VydC9pbnNlcnQuY29tcG9uZW50LnNhc3MifQ== */"

/***/ }),

/***/ "./src/app/insert/insert.component.ts":
/*!********************************************!*\
  !*** ./src/app/insert/insert.component.ts ***!
  \********************************************/
/*! exports provided: InsertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InsertComponent", function() { return InsertComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _Agent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Agent */ "./src/app/Agent.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");







let InsertComponent = class InsertComponent {
    constructor(http, route, router) {
        this.http = http;
        this.route = route;
        this.router = router;
        this.agent = new _Agent__WEBPACK_IMPORTED_MODULE_2__["Agent"]();
        this.showSuccess = false;
        this.showError = false;
        this.myControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.CustomersControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.customers = new Array();
        this.agent.Customers = new Array();
    }
    ngOnInit() {
        var customerUrl = "/api/Customer/";
        this.http.get(customerUrl)
            .subscribe(p => {
            this.customers = p.Items;
            this.filteredOptions = this.CustomersControl.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(value => this._filter(value)));
        });
    }
    Save(agent) {
        this.showSuccess = false;
        var agentUrl = "/api/Agent/Insert";
        this.http
            .post(agentUrl, agent)
            .toPromise()
            .then(res => {
            this.showSuccess = true;
        });
    }
    UpdateCustomer(option) {
        this.agent.Customers.push(option);
    }
    Cancel() {
        this.router.navigateByUrl("/home");
    }
    RemoveItem(customer) {
        const index = this.agent.Customers.indexOf(customer);
        if (index !== -1) {
            this.agent.Customers.splice(index, 1);
        }
    }
    _filter(value) {
        const filterValue = value.toString().toLowerCase();
        return this.customers.filter(option => option.CustomerName.toLowerCase().includes(filterValue));
    }
};
InsertComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
InsertComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-insert',
        template: __webpack_require__(/*! raw-loader!./insert.component.html */ "./node_modules/raw-loader/index.js!./src/app/insert/insert.component.html"),
        styles: [__webpack_require__(/*! ./insert.component.sass */ "./src/app/insert/insert.component.sass")]
    })
], InsertComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/inventory/coderush/Views/Agent/Agent/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map