(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "../../Customer/customers/src/app/Customer.ts":
/*!*************************************************************************************!*\
  !*** /var/www/html/inventory/coderush/Views/Customer/customers/src/app/Customer.ts ***!
  \*************************************************************************************/
/*! exports provided: Customer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Customer", function() { return Customer; });
var Customer = /** @class */ (function () {
    function Customer() {
    }
    return Customer;
}());



/***/ }),

/***/ "../../Pallet/pallet/src/app/pallet.ts":
/*!******************************************************************************!*\
  !*** /var/www/html/inventory/coderush/Views/Pallet/pallet/src/app/pallet.ts ***!
  \******************************************************************************/
/*! exports provided: Pallet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Pallet", function() { return Pallet; });
var Pallet = /** @class */ (function () {
    function Pallet() {
    }
    return Pallet;
}());



/***/ }),

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"content\">\n  <div class=\"box\">\n    <router-outlet></router-outlet>\n  </div>\n</section>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/edit/edit.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/edit/edit.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mb-3\" >\n        <div class=\"row ml-1 mr-1 mt-3 mb-5 text-light \">\n          <button\n            class=\"btn bg-secondary  btn-block col-md-3\"\n            [routerLink]=\"['../home']\"\n          >\n            back\n          </button>\n        </div>\n        <div class=\"table ml-3\" >\n          <div class=\"row\">\n             <div>                        \n                        <input\n                        type=\"text\"\n                        class=\"form-control ml-1 \"\n                        name=\"StateInvoiceNumber\"\n                        placeholder=\"State Invoice Number\"\n                        matInput\n                        [(ngModel)]=\"invoice.StateInvoiceNumber\"    \n                      />\n            </div>\n            <mat-form-field class=\"col\">\n              <input\n                type=\"text\"\n                class=\"col ml-1\"\n                name=\"invoice.Customer.CustomerId\"\n                placeholder=\"Customer Id\"\n                [formControl]= \"CustomerControl\"\n                matInput\n                [(ngModel)]=\"invoice.Customer.CustomerId\"\n                [matAutocomplete]=\"auto\"          \n              />\n              <label>{{ invoice.Customer.CustomerName }}</label>\n              <mat-autocomplete #auto=\"matAutocomplete\">\n                <mat-option\n                  *ngFor=\"let option of customerFilteredOptions | async\"  \n                  (click) = \"UpdateCustomer(option)\"\n                  [value]=\"option.CustomerId\"            \n                >\n                  {{ option.CustomerName }}\n                </mat-option>\n              </mat-autocomplete>\n            </mat-form-field>      \n            <mat-form-field class=\"col\">\n              <input\n                class=\"col ml-1\"\n                matInput          \n                [matDatepicker]=\"picker\"\n                [(ngModel)]=\"invoice.InvoiceDate\"\n                placeholder=\"Invoice Date\"\n              />\n              <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n              <mat-datepicker #picker></mat-datepicker>\n            </mat-form-field>\n            \n            \n          </div>\n          <div class=\"row\">\n          </div>\n        </div>\n        <table class=\"table table-striped able-sm mt-2\">\n          <thead class=\"thead-dark\">\n            <tr>\n              <th class=\"d-none d-sm-block\" scope=\"col\">#</th>\n              <th  scope=\"col\">Pallet Number</th>\n              <th  class=\"d-none d-sm-block\" scope=\"col\">Barcode</th>\n              <th scope=\"col\">Pallet Name</th>\n              <th scope=\"col\">Weight</th>\n              <th class=\"d-none d-sm-block\" scope=\"col\">Remaining Weight</th>\n              <th scope=\"col\">Sold weight</th>\n              <th scope=\"col\">Price</th>\n              <th scope=\"col\"></th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr\n              *ngFor=\"let invoicePallet of invoicePallets \"\n            >\n              <td class=\"d-none d-sm-block\">{{ invoicePallet.Pallet.PalletId }}</td>\n              <td >{{ invoicePallet.Pallet.PalletNumber }}</td>\n              <td  class=\"d-none d-sm-block\">{{ invoicePallet.Pallet.Barcode }}</td>\n              <td>{{ invoicePallet.Pallet.YarnType }} {{invoicePallet.Pallet.Denier}} {{invoicePallet.Pallet.Filament}} {{invoicePallet.Pallet.Intermingle}} {{invoicePallet.Pallet.ColorCode}} {{invoicePallet.Pallet.BobbinSize}} </td>\n              <td>{{ invoicePallet.Pallet.Weight }}</td>\n              <td class=\"d-none d-sm-block\">{{ invoicePallet.Pallet.RemainWeight }}</td>\n              <td><input class=\"form-control\" type=\"text\" [(ngModel)]=\"invoicePallet.Weight\" /></td>\n              <td >{{ invoicePallet.Pallet.Price }}</td>\n              <td ><input class=\"btn btn-danger\" (click)=\"RemoveItem(invoicePallet)\" type=\"button\" value=\"X\"/></td>\n            </tr>\n            <tr>\n              <td colspan=\"9\">\n                <mat-form-field class=\"col\">\n                  <input type=\"text\" class=\"ml-1 \" name=\"newPalletId\"\n                  placeholder=\"Add New Pallet\"\n                  matInput\n                  [formControl]= \"PalletsControl\"                \n                  [matAutocomplete]=\"autoPallet\"          \n                  />\n                <mat-autocomplete #autoPallet=\"matAutocomplete\">\n                <mat-option                      \n                  *ngFor=\"let palletOption of filteredOptions | async\"                              \n                  value=\"palletOption.PalletId\"\n                  (click)=\"Add(palletOption.PalletId)\"\n                >           \n                {{ palletOption.PalletNumber }} {{ palletOption.Barcode }} {{ palletOption.YarnType }} {{palletOption.Denier}} {{palletOption.Filament}} {{palletOption.Intermingle}} {{palletOption.ColorCode}} {{palletOption.BobbinSize}}\n                </mat-option>\n              </mat-autocomplete>\n            </mat-form-field>     \n            </td>\n           </tr>\n          </tbody>\n        </table>\n        <table class=\"table table-striped able-sm mt-2\">\n          <thead class=\"thead-dark\">\n            <tr>\n              <th class=\"d-none d-sm-block\" scope=\"col\">#</th>        \n              <th scope=\"col\">Barcode</th>\n              <th scope=\"col\">Box Name</th>\n              <th scope=\"col\">Weight</th>\n              <th class=\"d-none d-sm-block\" scope=\"col\">Remaining Weight</th>\n              <th scope=\"col\">Sold weight</th>\n              <th scope=\"col\">Price</th>\n              <th scope=\"col\"></th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor=\"let invoiceBox of invoiceBoxes \"\n          >\n            <td class=\"d-none d-sm-block\">{{ invoiceBox.Box.BoxId }}</td>      \n            <td>{{ invoiceBox.Box.Barcode }}</td>\n            <td>{{ invoiceBox.Box.YarnType }} {{invoiceBox.Box.Denier}} {{invoiceBox.Box.Filament}} {{invoiceBox.Box.Intermingle}} {{invoiceBox.Box.ColorCode}} {{invoiceBox.Box.BobbinSize}} </td>\n            <td>{{ invoiceBox.Box.Weight }}</td>\n            <td class=\"d-none d-sm-block\">{{ invoiceBox.Box.RemainWeight }}</td>\n            <td><input type=\"text\" [(ngModel)]=\"invoiceBox.Weight\" /></td>\n            <td >{{ invoiceBox.Box.Price }}</td>\n            <td ><input class=\"btn btn-danger\" (click)=\"RemoveBoxItem(invoiceBox)\" type=\"button\" value=\"X\"/></td>\n          </tr>\n          <tr>\n            <td colspan=\"9\">\n              <mat-form-field class=\"col\">\n                <input\n                  type=\"text\"\n                  class=\"ml-1 \"\n                  name=\"newBoxId\"\n                  placeholder=\"Add new Box\"\n                  matInput\n                  [formControl]= \"BoxesControl\"            \n                  [matAutocomplete]=\"autoBox\"          \n                />\n                <mat-autocomplete #autoBox=\"matAutocomplete\">\n                  <mat-option                      \n                    *ngFor=\"let boxOption of boxFilteredOptions | async\"                                 \n                    value=\"boxOption.BoxId\"\n                    (click)=\"AddBox(boxOption.BoxId)\"\n                  >           \n                  {{ boxOption.Barcode }} {{ boxOption.YarnType }} {{boxOption.Denier}} {{boxOption.Filament}} {{boxOption.Intermingle}} {{boxOption.ColorCode}} {{boxOption.BobbinSize}}\n                  </mat-option>\n                </mat-autocomplete>\n              </mat-form-field>  \n            </td>\n          </tr>\n          </tbody>\n        </table>\n      \n        <div>\n          <label>KDV %:</label>\n          <input\n          type=\"text\"\n          class=\"ml-1 form-control\"\n          name=\"invoice.KDV\"\n          placeholder=\"KDV\"        \n          [(ngModel)]=\"invoice.Kdv\"    \n        />\n        <label>Discount:</label>\n        <input\n        type=\"text\"\n        class=\"ml-1 form-control\"\n        name=\"invoice.Discount\"\n        placeholder=\"Discount\"\n        \n        [(ngModel)]=\"invoice.Discount\"\n      />\n        <label>Exchange Rate:</label>\n        <input\n          type=\"text\"\n          class=\"ml-1 \"\n          class=\"ml-1 form-control\"\n          placeholder=\"Exchange Rate\"          \n          [(ngModel)]=\"ExchangeRate\"    \n        />\n        <hr/>\n        <div class=\"table\" >\n        <div class=\"row\">\n          <div class=\"col\">\n        <label >KDV:</label><label class=\"ml-3\">{{Tax | number:'1.2-3'}}</label> \n      </div>\n      </div>\n      \n      <div class=\"row\">\n          <div class=\"col\">\n        <label >Total USD:</label><label class=\"ml-3\">{{TotalUsd | number:'1.2-3'}}</label>\n        </div>\n      </div>\n      <div class=\"row\">\n          <div class=\"col\">\n        <label >Total Lira:</label><label class=\"ml-3\">{{TotalLira | number:'1.2-3'}}</label>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"form-check form-check-inline ml-4\">\n          <input class=\"form-check-input\" type=\"radio\" name=\"inlineRadioOptions\" id=\"inlineRadio1\" [(ngModel)]=\"invoice.Currency\"  value=\"Usd\">\n          <label class=\"form-check-label\" for=\"inlineRadio1\"   >Usd</label>\n        </div>\n        <div class=\"form-check form-check-inline\">\n          <input class=\"form-check-input\" type=\"radio\" name=\"inlineRadioOptions\" id=\"inlineRadio2\" [(ngModel)]=\"invoice.Currency\" value=\"Lira\">\n          <label class=\"form-check-label\" for=\"inlineRadio2\"    >Lira</label>\n        </div>\n      </div>\n      <div class=\"row\">                  \n            <div class=\"col\">\n                <label >Total payment:</label>\n                <input\n                type=\"text\"\n                class=\"ml-1 form-control\"\n                name=\"invoice.Paid\"\n                placeholder=\"Total payment\"\n                [(ngModel)]=\"invoice.Paid\"\n              />\n          </div>\n        </div>\n      </div>\n        </div>\n        <p class=\"bg-success p-3 mb-2\" *ngIf=\"showSuccess\">Successfully Saved!</p>\n        <div class=\"row\">\n          <div class=\"col-md-2 mb-3\">\n            <button\n              class=\"btn btn-secondary btn-lg btn-block\"\n              type=\"button\"\n              (click)=\"Cancel()\"\n            >\n              Cancel\n            </button>\n          </div>\n          <div class=\"col-md-2 mb-3\">\n                <button\n                  class=\"btn btn-danger btn-lg btn-block\"\n                  type=\"button\"\n                  (click)=\"Delete()\"\n                >\n                  Delete\n                </button>\n              </div>\n          <div class=\"col-md-2 mb-3\">\n                  <a\n                    href=\"{{InvoiceReportUrl}}\"\n                    class=\"btn btn-primary btn-lg btn-block\"                    \n                    download\n                  >\n                    Download PDF\n                </a>\n          </div>\n          <div class=\"col-md-2 mb-3\">\n            <a\n              href=\"{{ExitReportUrl}}\"\n              class=\"btn btn-primary btn-lg btn-block\"                    \n              download\n            >\n              Download Exit Reports\n           </a>\n          </div>\n  \n          <div class=\"col-md-2 mb-3\">\n            <button\n              class=\"btn btn-success btn-lg btn-block\"\n              type=\"button\"\n              value=\"Save\"\n              (click)=\"Save(invoice)\"\n            >\n              Save\n            </button>\n          </div>\n        </div>\n      </div>\n      "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\" box-header with-border\">\n  <input\n    [(ngModel)]=\"searchText\"\n    placeholder=\"search text goes here\"\n    class=\"form-control col-md-3 ml-2 mb-3\"\n  />\n  <a\n    class=\"btn btn-success btn-md btn-block col-md-1 ml-2 mb-3\"\n    [routerLink]=\"['../insert']\"\n    >Add</a\n  >\n  <table class=\"table table-striped able-sm mt-2\">\n    <thead class=\"thead-dark\">\n      <tr>\n        <th class=\"hidden-xs\" (click)=\"Sort('InvoiceId')\" scope=\"col\">#</th>\n        <th (click)=\"Sort('CustomerName')\" scope=\"col\">Customer Name</th>\n        <th (click)=\"Sort('InvoiceDate')\" scope=\"col\">Invoice Date</th>\n        <th  (click)=\"Sort('TotalUsd')\" scope=\"col\">Total USD</th>\n        <th class=\"hidden-xs\" (click)=\"Sort('Paid')\" scope=\"col\">Total Payment</th>\n        <th  scope=\"col\">Difference</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr\n        *ngFor=\"let invoice of invoices | filter: searchText\"\n        (dblclick)=\"doubleClick($event)\"\n      >\n        <td class=\"hidden-xs\"  class=\"d-none d-sm-block\" scope=\"col\">{{invoice.InvoiceId}}</td>\n        <td  scope=\"col\">{{ invoice.Customer.CustomerName }}</td>\n        <td  scope=\"col\">{{ invoice.InvoiceDate | date }}</td>\n        <td  scope=\"col\">{{ invoice.TotalUsd | number:'1.2-3'}}</td>\n        <td class=\"hidden-xs\" [style.color]=\"getColor(invoice)\"  scope=\"col\">{{ invoice.Paid | number:'1.2-3'}}</td>\n        <td  [style.color]=\"getColor(invoice)\"  scope=\"col\">{{ getDifference(invoice) | number:'1.2-3'}}</td>\n      </tr>\n    </tbody>\n  </table>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/insert/insert.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/insert/insert.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mb-3\" >\n  <div class=\"row ml-1 mr-1 mt-3 mb-5 text-light \">\n    <button\n      class=\"btn bg-secondary  btn-block col-md-3\"\n      [routerLink]=\"['../home']\"\n    >\n      back\n    </button>\n  </div>\n  <div class=\"table ml-3\" >\n    <div class=\"row\">\n        <div>            \n            <input\n            type=\"text\"\n            class=\"form-control ml-1 \"\n            name=\"StateInvoiceNumber\"\n            placeholder=\"State Invoice Number\"\n            matInput\n            [(ngModel)]=\"invoice.StateInvoiceNumber\"    \n          />\n</div>\n      <mat-form-field class=\"col\">\n        <input\n          type=\"text\"\n          class=\"col ml-1\"\n          name=\"invoice.Customer.CustomerId\"\n          placeholder=\"Customer Id\"\n          [formControl]= \"CustomerControl\"\n          matInput          \n          [matAutocomplete]=\"auto\"          \n        />\n        <label>{{ invoice.Customer.CustomerName }}</label>\n        <mat-autocomplete #auto=\"matAutocomplete\">\n          <mat-option\n            *ngFor=\"let option of customerFilteredOptions | async\"            \n            (click) = \"UpdateCustomer(option)\"\n            [value]=\"option.CustomerId\"            \n          >\n            {{ option.CustomerName }}\n          </mat-option>\n        </mat-autocomplete>\n      </mat-form-field>      \n      <mat-form-field class=\"col\">\n        <input\n          class=\"col ml-1\"\n          matInput          \n          [matDatepicker]=\"picker\"\n          [(ngModel)]=\"invoice.InvoiceDate\"\n          placeholder=\"Invoice Date\"\n        />\n        <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n        <mat-datepicker #picker></mat-datepicker>\n      </mat-form-field>\n      \n      \n    </div>\n  </div>\n  <table class=\"table table-striped able-sm mt-2\">\n    <thead class=\"thead-dark\">\n      <tr>\n        <th class=\"d-none d-sm-block\" scope=\"col\">#</th>\n        <th scope=\"col\">Pallet Number</th>\n        <th class=\"d-none d-sm-block\" scope=\"col\">Barcode</th>\n        <th scope=\"col\">Pallet Name</th>\n        <th scope=\"col\">Weight</th>\n        <th class=\"d-none d-sm-block\" scope=\"col\">Remaining Weight</th>\n        <th scope=\"col\">Sold weight</th>\n        <th scope=\"col\">Price</th>\n        <th scope=\"col\"></th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr\n      *ngFor=\"let invoicePallet of invoicePallets \"\n    >\n      <td class=\"d-none d-sm-block\">{{ invoicePallet.Pallet.PalletId }}</td>\n      <td>{{ invoicePallet.Pallet.PalletNumber }}</td>\n      <td class=\"d-none d-sm-block\">{{ invoicePallet.Pallet.Barcode }}</td>\n      <td>{{ invoicePallet.Pallet.YarnType }} {{invoicePallet.Pallet.Denier}} {{invoicePallet.Pallet.Filament}} {{invoicePallet.Pallet.Intermingle}} {{invoicePallet.Pallet.ColorCode}} {{invoicePallet.Pallet.BobbinSize}} </td>\n      <td>{{ invoicePallet.Pallet.Weight }}</td>\n      <td class=\"d-none d-sm-block\">{{ invoicePallet.Pallet.RemainWeight }}</td>\n      <td><input class=\"form-control\" type=\"text\" [(ngModel)]=\"invoicePallet.Weight\" /></td>\n      <td >{{ invoicePallet.Pallet.Price }}</td>\n      <td ><input class=\"btn btn-danger\" (click)=\"RemoveItem(invoicePallet)\" type=\"button\" value=\"X\"/></td>\n    </tr>\n    <tr>\n      <td colspan=\"9\">\n        <mat-form-field class=\"col\">\n          <input\n            type=\"text\"\n            class=\"ml-1 \"\n            name=\"newPalletId\"\n            placeholder=\"Add new Pallet\"\n            matInput\n            [formControl]= \"PalletsControl\"            \n            [matAutocomplete]=\"autoPallet\"          \n          />\n          <mat-autocomplete #autoPallet=\"matAutocomplete\">\n            <mat-option                      \n              *ngFor=\"let palletOption of filteredOptions | async\"                                 \n              value=\"palletOption.PalletId\"\n              (click)=\"Add(palletOption.PalletId)\"\n            >           \n            {{ palletOption.PalletNumber }} {{ palletOption.Barcode }} {{ palletOption.YarnType }} {{palletOption.Denier}} {{palletOption.Filament}} {{palletOption.Intermingle}} {{palletOption.ColorCode}} {{palletOption.BobbinSize}}\n            </mat-option>\n          </mat-autocomplete>\n        </mat-form-field>  \n      </td>\n    </tr>\n    </tbody>\n  </table>\n  <table class=\"table table-striped able-sm mt-2\">\n    <thead class=\"thead-dark\">\n      <tr>\n        <th class=\"d-none d-sm-block\" scope=\"col\">#</th>        \n        <th scope=\"col\">Barcode</th>\n        <th scope=\"col\">Box Name</th>\n        <th scope=\"col\">Weight</th>\n        <th class=\"d-none d-sm-block\" scope=\"col\">Remaining Weight</th>\n        <th scope=\"col\">Sold weight</th>\n        <th scope=\"col\">Price</th>\n        <th scope=\"col\"></th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor=\"let invoiceBox of invoiceBoxes \"\n    >\n      <td class=\"d-none d-sm-block\">{{ invoiceBox.Box.BoxId }}</td>      \n      <td>{{ invoiceBox.Box.Barcode }}</td>\n      <td>{{ invoiceBox.Box.YarnType }} {{invoiceBox.Box.Denier}} {{invoiceBox.Box.Filament}} {{invoiceBox.Box.Intermingle}} {{invoiceBox.Box.ColorCode}} {{invoiceBox.Box.BobbinSize}} </td>\n      <td>{{ invoiceBox.Box.Weight }}</td>\n      <td class=\"d-none d-sm-block\">{{ invoiceBox.Box.RemainWeight }}</td>\n      <td><input type=\"text\" [(ngModel)]=\"invoiceBox.Weight\" /></td>\n      <td >{{ invoiceBox.Box.Price }}</td>\n      <td ><input class=\"btn btn-danger\" (click)=\"RemoveBoxItem(invoiceBox)\" type=\"button\" value=\"X\"/></td>\n    </tr>\n    <tr>\n      <td colspan=\"9\">\n        <mat-form-field class=\"col\">\n          <input\n            type=\"text\"\n            class=\"ml-1 \"\n            name=\"newBoxId\"\n            placeholder=\"Add new Box\"\n            matInput\n            [formControl]= \"BoxesControl\"            \n            [matAutocomplete]=\"autoBox\"          \n          />\n          <mat-autocomplete #autoBox=\"matAutocomplete\">\n            <mat-option                      \n              *ngFor=\"let boxOption of boxFilteredOptions | async\"    \n              value=\"boxOption.BoxId\"                             \n              (click)=\"AddBox(boxOption.BoxId)\"\n            >           \n            {{ boxOption.Barcode }} {{ boxOption.YarnType }} {{boxOption.Denier}} {{boxOption.Filament}} {{boxOption.Intermingle}} {{boxOption.ColorCode}} {{boxOption.BobbinSize}}\n            </mat-option>\n          </mat-autocomplete>\n        </mat-form-field>  \n      </td>\n    </tr>\n    </tbody>\n  </table>\n\n  <div>\n    <label>KDV %:</label>\n    <input\n    type=\"text\"\n    class=\"ml-1 form-control\"\n    name=\"invoice.Kdv\"\n    placeholder=\"KDV\"    \n    [(ngModel)]=\"invoice.Kdv\"    \n  />\n  <label>Discount:</label>\n  <input\n  type=\"text\"\n  class=\"ml-1 form-control \"\n  name=\"invoice.Discount\"\n  placeholder=\"Discount\"  \n  [(ngModel)]=\"invoice.Discount\"\n/>\n  <label>Exchange Rate:</label>\n  <input\n    type=\"text\"\n    class=\"ml-1 form-control\"\n    name=\"ExchangeRate\"\n    placeholder=\"Exchange Rate\"\n    [(ngModel)]=\"ExchangeRate\"    \n  />\n  <hr/>\n  <div class=\"table\" >\n  <div class=\"row\">\n    <div class=\"col\">\n  <label >KDV:</label><label class=\"ml-3\">{{Tax | number:'1.2-3'}}</label> \n</div>\n</div>\n<div class=\"row\">\n    <div class=\"col\">\n  <label >Total USD:</label><label class=\"ml-3\">{{TotalUsd | number:'1.2-3'}}</label>\n  </div>\n</div>\n<div class=\"row\">\n    <div class=\"col\">\n  <label >Total Lira:</label><label class=\"ml-3\">{{TotalLira | number:'1.2-3'}}</label>\n  </div>\n</div>\n<div class=\"row\">\n  <div class=\"form-check form-check-inline ml-4\">\n    <input class=\"form-check-input\" type=\"radio\" name=\"inlineRadioOptions\" id=\"inlineRadio1\" value=\"option1\">\n    <label class=\"form-check-label\" for=\"inlineRadio1\"  [(ngModel)]=\"invoice.Currency\"  >Usd</label>\n  </div>\n  <div class=\"form-check form-check-inline\">\n    <input class=\"form-check-input\" type=\"radio\" name=\"inlineRadioOptions\" id=\"inlineRadio2\" value=\"option2\">\n    <label class=\"form-check-label\" for=\"inlineRadio2\"  [(ngModel)]=\"invoice.Currency\"  >Lira</label>\n  </div>\n</div>\n<div class=\"row\">\n    <div class=\"col\">\n        <label >Total payment:</label>\n        <input\n        type=\"text\"\n        class=\"ml-1 form-control\"\n        name=\"invoice.Paid\"\n        placeholder=\"Total payment\"\n        [(ngModel)]=\"invoice.Paid\"\n      />\n  </div>\n</div>\n\n</div>\n  </div>\n  <p class=\"bg-success p-3 mb-2\" *ngIf=\"showSuccess\">Successfully Saved!</p>\n  <div class=\"row\">\n    <div class=\"col-md-3 mb-3\">\n      <button\n        class=\"btn btn-secondary btn-lg btn-block\"\n        type=\"button\"\n        (click)=\"Cancel()\"\n      >\n        Cancel\n      </button>\n    </div>\n    <div class=\"col-md-3 mb-3\">\n      <button\n        class=\"btn btn-success btn-lg btn-block\"\n        type=\"button\"\n        value=\"Save\"\n        (click)=\"Save(invoice)\"\n      >\n        Save\n      </button>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/Invoice.ts":
/*!****************************!*\
  !*** ./src/app/Invoice.ts ***!
  \****************************/
/*! exports provided: Invoice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Invoice", function() { return Invoice; });
var Invoice = /** @class */ (function () {
    function Invoice() {
    }
    return Invoice;
}());



/***/ }),

/***/ "./src/app/InvoiceBox.ts":
/*!*******************************!*\
  !*** ./src/app/InvoiceBox.ts ***!
  \*******************************/
/*! exports provided: InvoiceBox */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoiceBox", function() { return InvoiceBox; });
var InvoiceBox = /** @class */ (function () {
    function InvoiceBox() {
        this.Weight = 0;
        this.InvoiceId = 0;
        this.BoxId = 0;
    }
    return InvoiceBox;
}());



/***/ }),

/***/ "./src/app/InvoicePallet.ts":
/*!**********************************!*\
  !*** ./src/app/InvoicePallet.ts ***!
  \**********************************/
/*! exports provided: InvoicePallet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicePallet", function() { return InvoicePallet; });
var InvoicePallet = /** @class */ (function () {
    function InvoicePallet() {
        this.Weight = 0;
        this.InvoiceId = 0;
        this.PalletId = 0;
    }
    return InvoicePallet;
}());



/***/ }),

/***/ "./src/app/app.component.sass":
/*!************************************!*\
  !*** ./src/app/app.component.sass ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Invoice';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.sass */ "./src/app/app.component.sass")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _filter_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./filter.pipe */ "./src/app/filter.pipe.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _insert_insert_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./insert/insert.component */ "./src/app/insert/insert.component.ts");
/* harmony import */ var _edit_edit_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./edit/edit.component */ "./src/app/edit/edit.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/esm5/autocomplete.es5.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");

















var appRoutes = [
    { path: "home", component: _home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"] },
    { path: "edit", component: _edit_edit_component__WEBPACK_IMPORTED_MODULE_7__["EditComponent"] },
    { path: "insert", component: _insert_insert_component__WEBPACK_IMPORTED_MODULE_6__["InsertComponent"] },
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "**", component: _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _filter_pipe__WEBPACK_IMPORTED_MODULE_3__["FilterPipe"],
                _edit_edit_component__WEBPACK_IMPORTED_MODULE_7__["EditComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"],
                _insert_insert_component__WEBPACK_IMPORTED_MODULE_6__["InsertComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"].forRoot(appRoutes),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_14__["MatFormFieldModule"],
                _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_11__["MatAutocompleteModule"],
                _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatNativeDateModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatInputModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ReactiveFormsModule"]
            ],
            exports: [
                _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_11__["MatAutocompleteModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_14__["MatFormFieldModule"],
                _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatInputModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ReactiveFormsModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/edit/edit.component.sass":
/*!******************************************!*\
  !*** ./src/app/edit/edit.component.sass ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VkaXQvZWRpdC5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/edit/edit.component.ts":
/*!****************************************!*\
  !*** ./src/app/edit/edit.component.ts ***!
  \****************************************/
/*! exports provided: EditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditComponent", function() { return EditComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Invoice__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Invoice */ "./src/app/Invoice.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _Customer_customers_src_app_Customer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../Customer/customers/src/app/Customer */ "../../Customer/customers/src/app/Customer.ts");
/* harmony import */ var _Pallet_pallet_src_app_pallet__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../Pallet/pallet/src/app/pallet */ "../../Pallet/pallet/src/app/pallet.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _InvoicePallet__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../InvoicePallet */ "./src/app/InvoicePallet.ts");
/* harmony import */ var _InvoiceBox__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../InvoiceBox */ "./src/app/InvoiceBox.ts");











var EditComponent = /** @class */ (function () {
    function EditComponent(http, route, router) {
        this.http = http;
        this.route = route;
        this.router = router;
        this.pallet = new _Invoice__WEBPACK_IMPORTED_MODULE_4__["Invoice"]();
        this.showSuccess = false;
        this.showError = false;
        this.CustomerControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]();
        this.PalletsControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]();
        this.BoxesControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]();
        this.invoice = new _Invoice__WEBPACK_IMPORTED_MODULE_4__["Invoice"]();
        this.customers = new Array();
        this.pallets = new Array();
        this.boxes = new Array();
        this.newPallet = new _Pallet_pallet_src_app_pallet__WEBPACK_IMPORTED_MODULE_7__["Pallet"]();
        this.invoicePallets = new Array();
        this.invoiceBoxes = new Array();
        this.invoice.Customer = new _Customer_customers_src_app_Customer__WEBPACK_IMPORTED_MODULE_6__["Customer"]();
        this.invoice.Kdv = 8;
        this.TotalUsd = 0;
        this.TotalLira = 0;
        this.ExchangeRate = 5.7;
    }
    EditComponent.prototype.ngOnInit = function () {
        var _this = this;
        var customerUrl = "/api/Customer/";
        var palletUrl = "/api/Pallet/GetAvailablePallets";
        var boxUrl = "/api/Box/GetAvailableBoxes";
        var invoiceUrl = "/api/Invoice/GetInvoice/";
        var id = this.route.snapshot.queryParamMap.get("id");
        this.http.get(customerUrl)
            .subscribe(function (p) {
            _this.customers = p.Items;
            _this.customerFilteredOptions = _this.CustomerControl.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (value) { return _this._customerFilter(value); }));
        });
        this.http.get(palletUrl)
            .subscribe(function (p) {
            _this.pallets = p.Items;
            _this.filteredOptions = _this.PalletsControl.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (value) { return _this._filter(value); }));
        });
        this.http.get(boxUrl)
            .subscribe(function (p) {
            _this.boxes = p.Items;
            _this.boxFilteredOptions = _this.BoxesControl.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (value) { return _this._boxFilter(value); }));
        });
        this.http.get(invoiceUrl + id)
            .subscribe(function (p) {
            _this.invoice = p;
            for (var i = 0; i < p.InvoicePallets.length; i++) {
                _this.invoicePallets.push(p.InvoicePallets[i]);
            }
            for (var i = 0; i < p.InvoiceBoxes.length; i++) {
                _this.invoiceBoxes.push(p.InvoiceBoxes[i]);
            }
            _this.ExchangeRate = _this.invoice.ExchangeRate;
            _this.TotalUsd = _this.invoice.TotalUsd;
            _this.Tax = _this.invoice.Tax;
            _this.Calculate();
        });
        this.InvoiceReportUrl = "/InvoiceReport/print/?id=" + id;
        this.ExitReportUrl = "/WarehouseTakeOutReport/print/?invoiceId=" + id;
    };
    EditComponent.prototype.Add = function (newPalletId) {
        var selectedPallet = this.pallets.find(function (p) { return p.PalletId == newPalletId; });
        var newInvoicePallet = new _InvoicePallet__WEBPACK_IMPORTED_MODULE_9__["InvoicePallet"]();
        newInvoicePallet.PalletId = selectedPallet.PalletId;
        newInvoicePallet.Pallet = selectedPallet;
        newInvoicePallet.InvoiceId = this.invoice.InvoiceId;
        newInvoicePallet.Invoice = this.invoice;
        newInvoicePallet.Weight = selectedPallet.RemainWeight;
        this.invoicePallets.push(newInvoicePallet);
        this.Calculate();
    };
    EditComponent.prototype._filter = function (value) {
        var filterValue = value.toString().toLowerCase();
        return this.pallets.filter(function (option) { return option.PalletNumber.toString().includes(filterValue) ||
            option.YarnType.includes(filterValue) ||
            option.Denier.toString().includes(filterValue) ||
            option.Filament.toString().includes(filterValue); });
    };
    EditComponent.prototype._boxFilter = function (value) {
        var filterValue = value.toString().toLowerCase();
        if (!value)
            filterValue = "";
        return this.boxes.filter(function (option) { return option.Barcode.toString().includes(filterValue) ||
            option.YarnType.includes(filterValue) ||
            option.Denier.toString().includes(filterValue) ||
            option.Filament.toString().includes(filterValue); });
    };
    EditComponent.prototype._customerFilter = function (value) {
        var filterValue = value.toString().toLowerCase();
        return this.customers.filter(function (option) { return option.CustomerName.toLocaleLowerCase().includes(filterValue); });
    };
    EditComponent.prototype.Delete = function () {
        var _this = this;
        this.showSuccess = false;
        var invoiceUrl = "/api/Invoice/Delete/?invoiceId=" + this.invoice.InvoiceId;
        this.http
            .delete(invoiceUrl)
            .toPromise()
            .then(function (res) {
            _this.showSuccess = true;
            _this.router.navigateByUrl("/home");
        });
    };
    EditComponent.prototype.Download = function () {
        var invoiceUrl = "/InvoiceReport/index/?invoiceId=" + this.invoice.InvoiceId;
        this.http
            .get(invoiceUrl)
            .toPromise()
            .then();
    };
    EditComponent.prototype.RemoveItem = function (selectedPallet) {
        var index = this.invoicePallets.indexOf(selectedPallet);
        if (index !== -1) {
            this.invoicePallets.splice(index, 1);
        }
        this.Calculate();
    };
    EditComponent.prototype.AddBox = function (newBoxId) {
        var selectedBox = this.boxes.find(function (p) { return p.BoxId == newBoxId; });
        var newInvoiceBox = new _InvoiceBox__WEBPACK_IMPORTED_MODULE_10__["InvoiceBox"]();
        newInvoiceBox.BoxId = selectedBox.BoxId;
        newInvoiceBox.Box = selectedBox;
        newInvoiceBox.InvoiceId = this.invoice.InvoiceId;
        newInvoiceBox.Invoice = this.invoice;
        newInvoiceBox.Weight = selectedBox.RemainWeight;
        this.invoiceBoxes.push(newInvoiceBox);
        this.Calculate();
    };
    EditComponent.prototype.RemoveBoxItem = function (selectedBox) {
        var index = this.invoiceBoxes.indexOf(selectedBox);
        if (index !== -1) {
            this.invoiceBoxes.splice(index, 1);
        }
        this.Calculate();
    };
    EditComponent.prototype.Calculate = function () {
        this.TotalUsd = 0;
        for (var i = 0; i < this.invoicePallets.length; i++) {
            var weight = this.invoicePallets[i].Weight;
            this.TotalUsd = weight * this.invoicePallets[i].Pallet.Price + this.TotalUsd;
        }
        for (var i = 0; i < this.invoiceBoxes.length; i++) {
            var weight = this.invoiceBoxes[i].Weight;
            this.TotalUsd = weight * this.invoiceBoxes[i].Box.Price + this.TotalUsd;
        }
        this.TotalUsd = this.TotalUsd - this.invoice.Discount;
        this.TotalUsd = Math.round(this.TotalUsd * 1000) / 1000;
        this.Tax = this.TotalUsd * this.invoice.Kdv / 100;
        this.Tax = Math.round(this.Tax * 1000) / 1000;
        this.TotalUsd = this.TotalUsd + this.Tax;
        this.TotalLira = this.TotalUsd * this.ExchangeRate;
        this.TotalLira = Math.round(this.TotalLira * 1000) / 1000;
    };
    EditComponent.prototype.Save = function (invoice) {
        var _this = this;
        this.Calculate();
        this.showSuccess = false;
        var invoiceURL = "/api/Invoice/Update";
        this.invoice.InvoicePallets = new Array();
        for (var i = 0; i < this.invoicePallets.length; i++) {
            var invoicePallet = new _InvoicePallet__WEBPACK_IMPORTED_MODULE_9__["InvoicePallet"]();
            invoicePallet.InvoiceId = this.invoice.InvoiceId;
            invoicePallet.PalletId = this.invoicePallets[i].PalletId;
            invoicePallet.Weight = this.invoicePallets[i].Weight;
            invoice.InvoicePallets.push(invoicePallet);
        }
        this.invoice.InvoiceBoxes = new Array();
        for (var i = 0; i < this.invoiceBoxes.length; i++) {
            var invoiceBox = new _InvoiceBox__WEBPACK_IMPORTED_MODULE_10__["InvoiceBox"]();
            invoiceBox.InvoiceId = this.invoice.InvoiceId;
            invoiceBox.BoxId = this.invoiceBoxes[i].BoxId;
            invoiceBox.Weight = this.invoiceBoxes[i].Weight;
            invoice.InvoiceBoxes.push(invoiceBox);
        }
        invoice.ExchangeRate = this.ExchangeRate;
        invoice.TotalUsd = this.TotalUsd;
        invoice.Tax = this.Tax;
        this.http
            .post(invoiceURL, invoice)
            .toPromise()
            .then(function (res) {
            _this.showSuccess = true;
        });
    };
    EditComponent.prototype.UpdateCustomer = function (option) {
        this.invoice.Customer = option;
    };
    EditComponent.prototype.Cancel = function () {
        this.router.navigateByUrl("/home");
    };
    EditComponent.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    EditComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__(/*! raw-loader!./edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/edit/edit.component.html"),
            styles: [__webpack_require__(/*! ./edit.component.sass */ "./src/app/edit/edit.component.sass")]
        })
    ], EditComponent);
    return EditComponent;
}());



/***/ }),

/***/ "./src/app/filter.pipe.ts":
/*!********************************!*\
  !*** ./src/app/filter.pipe.ts ***!
  \********************************/
/*! exports provided: FilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterPipe", function() { return FilterPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (items, searchText) {
        if (!items)
            return [];
        if (!searchText)
            return items;
        searchText = searchText.toString().toLowerCase();
        return items.filter(function (it) {
            return JSON.stringify(it)
                .toLowerCase()
                .includes(searchText);
        });
    };
    FilterPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: "filter"
        })
    ], FilterPipe);
    return FilterPipe;
}());



/***/ }),

/***/ "./src/app/home/home.component.sass":
/*!******************************************!*\
  !*** ./src/app/home/home.component.sass ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");




var HomeComponent = /** @class */ (function () {
    function HomeComponent(http, router) {
        this.http = http;
        this.router = router;
        this.invoices = new Array();
        this.preventSingleClick = false;
        this.sortUp = new Array();
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        var invoiceURL = "/api/Invoice/";
        this.http
            .get(invoiceURL)
            .toPromise()
            .then(function (res) {
            _this.invoices = res["Items"];
        });
    };
    HomeComponent.prototype.doubleClick = function (event) {
        this.preventSingleClick = true;
        clearTimeout(this.timer);
        //Navigate on double click
        this.router.navigate(["/edit/"], {
            queryParams: { id: event.currentTarget.children[0].innerText }
        });
    };
    HomeComponent.prototype.getColor = function (invoice) {
        var lira = invoice.TotalUsd * invoice.ExchangeRate;
        var isLira = false;
        var diffLira = invoice.Paid - lira;
        var diffUsd = invoice.Paid - invoice.TotalUsd;
        if (invoice.Currency === 'Lira')
            isLira = true;
        if (isLira === false) {
            if (diffUsd < -1)
                return "red";
            if (diffUsd > 1)
                return "green";
        }
        else {
            if (diffLira < -1)
                return "red";
            if (diffLira > 1)
                return "green";
        }
        return "";
    };
    HomeComponent.prototype.getDifference = function (invoice) {
        var lira = invoice.TotalUsd * invoice.ExchangeRate;
        var diffLira = invoice.Paid - lira;
        var diffUsd = invoice.Paid - invoice.TotalUsd;
        if (invoice.Currency === 'Lira')
            return diffLira;
        else
            return diffUsd;
    };
    HomeComponent.prototype.Sort = function (sortHeader) {
        switch (sortHeader) {
            case 'InvoiceId':
                {
                    this.sortUp[0] = !this.sortUp[0];
                    if (this.sortUp[0]) {
                        this.invoices.sort(function (a, b) { return a.InvoiceId - b.InvoiceId; });
                    }
                    else {
                        this.invoices.sort(function (a, b) { return b.InvoiceId - a.InvoiceId; });
                    }
                    break;
                }
            case 'CustomerName':
                {
                    this.sortUp[1] = !this.sortUp[1];
                    if (this.sortUp[1]) {
                        this.invoices.sort(function (a, b) { return a.Customer.CustomerName.toLocaleLowerCase().localeCompare(b.Customer.CustomerName.toLocaleLowerCase()); });
                    }
                    else {
                        this.invoices.sort(function (a, b) { return b.Customer.CustomerName.toLocaleLowerCase().localeCompare(a.Customer.CustomerName.toLocaleLowerCase()); });
                    }
                    break;
                }
            case 'InvoiceDate':
                {
                    this.sortUp[2] = !this.sortUp[2];
                    if (this.sortUp[2]) {
                        this.invoices.sort(function (a, b) { return a.InvoiceDate.localeCompare(b.InvoiceDate); });
                    }
                    else {
                        this.invoices.sort(function (a, b) { return b.InvoiceDate.localeCompare(a.InvoiceDate); });
                    }
                    break;
                }
            case 'TotalUsd':
                {
                    this.sortUp[4] = !this.sortUp[4];
                    if (this.sortUp[4]) {
                        this.invoices.sort(function (a, b) { return a.TotalUsd - b.TotalUsd; });
                    }
                    else {
                        this.invoices.sort(function (a, b) { return b.TotalUsd - a.TotalUsd; });
                    }
                    break;
                }
            case 'Paid':
                {
                    this.sortUp[5] = !this.sortUp[5];
                    if (this.sortUp[5]) {
                        this.invoices.sort(function (a, b) { return a.Paid - b.Paid; });
                    }
                    else {
                        this.invoices.sort(function (a, b) { return b.Paid - a.Paid; });
                    }
                    break;
                }
        }
    };
    HomeComponent.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-home",
            template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.sass */ "./src/app/home/home.component.sass")]
        })
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/insert/insert.component.sass":
/*!**********************************************!*\
  !*** ./src/app/insert/insert.component.sass ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2luc2VydC9pbnNlcnQuY29tcG9uZW50LnNhc3MifQ== */"

/***/ }),

/***/ "./src/app/insert/insert.component.ts":
/*!********************************************!*\
  !*** ./src/app/insert/insert.component.ts ***!
  \********************************************/
/*! exports provided: InsertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InsertComponent", function() { return InsertComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Invoice__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Invoice */ "./src/app/Invoice.ts");
/* harmony import */ var _InvoicePallet__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../InvoicePallet */ "./src/app/InvoicePallet.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _Customer_customers_src_app_Customer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../Customer/customers/src/app/Customer */ "../../Customer/customers/src/app/Customer.ts");
/* harmony import */ var _Pallet_pallet_src_app_pallet__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../Pallet/pallet/src/app/pallet */ "../../Pallet/pallet/src/app/pallet.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _InvoiceBox__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../InvoiceBox */ "./src/app/InvoiceBox.ts");











var InsertComponent = /** @class */ (function () {
    function InsertComponent(http, route, router) {
        this.http = http;
        this.route = route;
        this.router = router;
        this.showSuccess = false;
        this.showError = false;
        this.CustomerControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]();
        this.PalletsControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]();
        this.BoxesControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]();
        this.invoice = new _Invoice__WEBPACK_IMPORTED_MODULE_4__["Invoice"]();
        this.customers = new Array();
        this.pallets = new Array();
        this.boxes = new Array();
        this.newPallet = new _Pallet_pallet_src_app_pallet__WEBPACK_IMPORTED_MODULE_8__["Pallet"]();
        this.invoicePallets = new Array();
        this.invoiceBoxes = new Array();
        this.invoice.Customer = new _Customer_customers_src_app_Customer__WEBPACK_IMPORTED_MODULE_7__["Customer"]();
        this.invoice.InvoicePallets = new Array();
        this.invoice.InvoiceBoxes = new Array();
    }
    InsertComponent.prototype.ngOnInit = function () {
        var _this = this;
        var customerUrl = "/api/Customer/";
        var palletUrl = "/api/Pallet/GetAvailablePallets";
        var boxUrl = "/api/Box/GetAvailableBoxes";
        this.exchangeApi = "/api/exchange/";
        this.http.get(customerUrl)
            .subscribe(function (p) {
            _this.customers = p.Items;
            _this.customerFilteredOptions = _this.CustomerControl.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])(function (value) { return _this._customerFilter(value); }));
        });
        this.http.get(palletUrl)
            .subscribe(function (p) {
            _this.pallets = p.Items;
            _this.filteredOptions = _this.PalletsControl.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])(function (value) { return _this._filter(value); }));
        });
        this.http.get(boxUrl)
            .subscribe(function (p) {
            _this.boxes = p.Items;
            _this.boxFilteredOptions = _this.BoxesControl.valueChanges
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_9__["map"])(function (value) { return _this._boxFilter(value); }));
        });
        this.http.get(this.exchangeApi)
            .subscribe(function (p) {
            _this.ExchangeRate = p;
        });
        this.invoice.Kdv = 8;
        this.invoice.Discount = 0;
        this.TotalUsd = 0;
        this.TotalLira = 0;
        this.ExchangeRate = 5.7;
    };
    InsertComponent.prototype.Add = function (newPalletId) {
        var selectedPallet = this.pallets.find(function (p) { return p.PalletId == newPalletId; });
        var newInvoicePallet = new _InvoicePallet__WEBPACK_IMPORTED_MODULE_5__["InvoicePallet"]();
        newInvoicePallet.PalletId = selectedPallet.PalletId;
        newInvoicePallet.Pallet = selectedPallet;
        newInvoicePallet.InvoiceId = this.invoice.InvoiceId;
        newInvoicePallet.Invoice = this.invoice;
        newInvoicePallet.Weight = selectedPallet.RemainWeight;
        this.invoicePallets.push(newInvoicePallet);
        this.Calculate();
    };
    InsertComponent.prototype.RemoveItem = function (selectedPallet) {
        var index = this.invoicePallets.indexOf(selectedPallet);
        if (index !== -1) {
            this.invoicePallets.splice(index, 1);
        }
        this.Calculate();
    };
    InsertComponent.prototype.AddBox = function (newBoxId) {
        var selectedBox = this.boxes.find(function (p) { return p.BoxId == newBoxId; });
        var newInvoiceBox = new _InvoiceBox__WEBPACK_IMPORTED_MODULE_10__["InvoiceBox"]();
        newInvoiceBox.BoxId = selectedBox.BoxId;
        newInvoiceBox.Box = selectedBox;
        newInvoiceBox.InvoiceId = this.invoice.InvoiceId;
        newInvoiceBox.Invoice = this.invoice;
        newInvoiceBox.Weight = selectedBox.RemainWeight;
        this.invoiceBoxes.push(newInvoiceBox);
        this.Calculate();
    };
    InsertComponent.prototype.RemoveBoxItem = function (selectedBox) {
        var index = this.invoiceBoxes.indexOf(selectedBox);
        if (index !== -1) {
            this.invoiceBoxes.splice(index, 1);
        }
        this.Calculate();
    };
    InsertComponent.prototype.Calculate = function () {
        this.TotalUsd = 0;
        for (var i = 0; i < this.invoicePallets.length; i++) {
            var weight = this.invoicePallets[i].Weight;
            this.TotalUsd = weight * this.invoicePallets[i].Pallet.Price + this.TotalUsd;
        }
        for (var i = 0; i < this.invoiceBoxes.length; i++) {
            var weight = this.invoiceBoxes[i].Weight;
            this.TotalUsd = weight * this.invoiceBoxes[i].Box.Price + this.TotalUsd;
        }
        this.TotalUsd = this.TotalUsd - this.invoice.Discount;
        this.TotalUsd = Math.round(this.TotalUsd * 1000) / 1000;
        this.Tax = this.TotalUsd * this.invoice.Kdv / 100;
        this.Tax = Math.round(this.Tax * 1000) / 1000;
        this.TotalUsd = this.TotalUsd + this.Tax;
        this.TotalLira = this.TotalUsd * this.ExchangeRate;
        this.TotalLira = Math.round(this.TotalLira * 1000) / 1000;
    };
    InsertComponent.prototype._filter = function (value) {
        var filterValue = value.toString().toLowerCase();
        return this.pallets.filter(function (option) { return option.PalletNumber.toString().includes(filterValue) ||
            option.YarnType.includes(filterValue) ||
            option.Denier.toString().includes(filterValue) ||
            option.Filament.toString().includes(filterValue); });
    };
    InsertComponent.prototype._boxFilter = function (value) {
        var filterValue = value.toString().toLowerCase();
        if (!value)
            filterValue = "";
        return this.boxes.filter(function (option) { return option.Barcode.toString().includes(filterValue) ||
            option.YarnType.includes(filterValue) ||
            option.Denier.toString().includes(filterValue) ||
            option.Filament.toString().includes(filterValue); });
    };
    InsertComponent.prototype._customerFilter = function (value) {
        var filterValue = value.toString().toLowerCase();
        return this.customers.filter(function (option) { return option.CustomerName.toLocaleLowerCase().includes(filterValue); });
    };
    InsertComponent.prototype.Save = function (invoice) {
        var _this = this;
        this.Calculate();
        this.showSuccess = false;
        var invoiceURL = "/api/Invoice/Insert";
        invoice.InvoicePallets = new Array();
        for (var i = 0; i < this.invoicePallets.length; i++) {
            var invoicePallet = new _InvoicePallet__WEBPACK_IMPORTED_MODULE_5__["InvoicePallet"]();
            invoicePallet.PalletId = this.invoicePallets[i].PalletId;
            invoicePallet.Weight = this.invoicePallets[i].Weight;
            invoicePallet.Pallet = this.invoicePallets[i].Pallet;
            invoicePallet.Invoice = new _Invoice__WEBPACK_IMPORTED_MODULE_4__["Invoice"]();
            invoicePallet.InvoiceId = 0;
            invoice.InvoicePallets.push(invoicePallet);
        }
        for (var i = 0; i < this.invoiceBoxes.length; i++) {
            var invoiceBox = new _InvoiceBox__WEBPACK_IMPORTED_MODULE_10__["InvoiceBox"]();
            invoiceBox.BoxId = this.invoiceBoxes[i].BoxId;
            invoiceBox.Weight = this.invoiceBoxes[i].Weight;
            invoiceBox.Box = this.invoiceBoxes[i].Box;
            invoiceBox.Invoice = new _Invoice__WEBPACK_IMPORTED_MODULE_4__["Invoice"]();
            invoiceBox.InvoiceId = 0;
            invoice.InvoiceBoxes.push(invoiceBox);
        }
        invoice.ExchangeRate = this.ExchangeRate;
        invoice.TotalUsd = this.TotalUsd;
        invoice.Tax = this.Tax;
        this.http
            .post(invoiceURL, invoice)
            .toPromise()
            .then(function (res) {
            _this.showSuccess = true;
        });
    };
    InsertComponent.prototype.UpdateCustomer = function (option) {
        this.invoice.Customer = option;
    };
    InsertComponent.prototype.Cancel = function () {
        this.router.navigateByUrl("/home");
    };
    InsertComponent.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    InsertComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-insert",
            template: __webpack_require__(/*! raw-loader!./insert.component.html */ "./node_modules/raw-loader/index.js!./src/app/insert/insert.component.html"),
            styles: [__webpack_require__(/*! ./insert.component.sass */ "./src/app/insert/insert.component.sass")]
        })
    ], InsertComponent);
    return InsertComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/inventory/coderush/Views/Invoice/Invoice/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map