(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"content\">\n  <div class=\"box\">\n    <router-outlet></router-outlet>\n  </div>\n</section>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/edit/edit.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/edit/edit.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mb-3\">\n  <div class=\"row ml-1 mr-1 mt-3 mb-5 text-light \">\n    <button\n      class=\"btn bg-secondary  btn-block col-md-3\"\n      [routerLink]=\"['../index']\"\n    >\n      back\n    </button>\n  </div>\n  <form>\n    <div class=\"mb-3\">\n      <label for=\"customer.CustomerName\">Customer name:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.CustomerName\"\n        name=\"customer.CustomerNamer\"\n        type=\"text\"\n        [(ngModel)]=\"customer.CustomerName\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.Address\">Address:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.Address\"\n        name=\"customer.Address\"\n        type=\"text\"\n        [(ngModel)]=\"customer.Address\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.Address\">City:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.City\"\n        name=\"customer.City\"\n        type=\"text\"\n        [(ngModel)]=\"customer.City\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.ZipCode\">ZipCode:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.ZipCode\"\n        name=\"customer.ZipCode\"\n        type=\"text\"\n        [(ngModel)]=\"customer.ZipCode\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.Phone\">Phone:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.Phone\"\n        name=\"customer.Phone\"\n        type=\"text\"\n        [(ngModel)]=\"customer.Phone\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.MobilePhone\">Mobile Phone:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.MobilePhone\"\n        name=\"customer.MobilePhone\"\n        type=\"text\"\n        [(ngModel)]=\"customer.MobilePhone\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.Email\">Email:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.Email\"\n        name=\"customer.Email\"\n        type=\"text\"\n        [(ngModel)]=\"customer.Email\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.Website\">Website:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.Website\"\n        name=\"customer.Website\"\n        type=\"text\"\n        [(ngModel)]=\"customer.Website\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.Website\">Vergi:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.Vergi\"\n        name=\"customer.Vergi\"\n        type=\"text\"\n        [(ngModel)]=\"customer.Vergi\"\n      />\n    </div>\n    <p class=\"bg-success p-3 mb-2\" *ngIf=\"showSuccess\">Successfully Saved!</p>\n    <div class=\"row\">\n      <div class=\"col-md-3 mb-3\">\n        <button\n          class=\"btn btn-secondary btn-lg btn-block\"\n          type=\"button\"\n          (click)=\"Cancel()\"\n        >\n          Cancel\n        </button>\n      </div>\n      <div class=\"col-md-3 mb-3\">\n        <button\n          class=\"btn btn-danger btn-lg btn-block\"\n          type=\"button\"\n          (click)=\"Delete(customer)\"\n        >\n          Delete\n        </button>\n      </div>\n      <div class=\"col-md-3 mb-3\">\n        <button\n          class=\"btn btn-success btn-lg btn-block\"\n          type=\"button\"\n          value=\"Save\"\n          (click)=\"Save(customer)\"\n        >\n          Save\n        </button>\n      </div>\n    </div>\n  </form>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\" box-header with-border\">\n  <input\n    [(ngModel)]=\"searchText\"\n    placeholder=\"search text goes here\"\n    class=\"form-control col-md-3 ml-2 mb-3\"\n  />\n  <a\n    class=\"btn btn-success btn-md btn-block col-md-1 ml-2 mb-3\"\n    [routerLink]=\"['../insert']\"\n    >Add</a\n  >\n  <table class=\"table table-striped able-sm mt-2\">\n    <thead class=\"thead-dark\">\n      <tr>\n        <th class=\"d-none d-sm-block\" scope=\"col\">#</th>\n        <th scope=\"col\">Customer Name</th>\n        <th scope=\"col\">Address</th>\n        <th class=\"hidden-sm hidden-xs\" scope=\"col\">City</th>\n        <th class=\"hidden-sm hidden-xs\" scope=\"col\">Zip Code</th>\n        <th class=\"hidden-sm hidden-xs\" scope=\"col\">Phone</th>\n        <th scope=\"col\">Mobile Phone</th>\n        <th class=\"hidden-sm hidden-xs\" scope=\"col\">Email</th>\n        <th class=\"d-none d-sm-block\" scope=\"col\">Website</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr\n        *ngFor=\"let customer of customers | filter: searchText\"\n        (click)=\"singleClick($event)\"\n        (dblclick)=\"doubleClick($event)\"\n      >\n        <td class=\"d-none d-sm-block\" scope=\"col\">{{ customer.CustomerId }}</td>\n        <td scope=\"col\">{{ customer.CustomerName }}</td>\n        <td scope=\"col\">{{ customer.Address }}</td>\n        <td class=\"hidden-sm hidden-xs\" scope=\"col\">{{ customer.City }}</td>\n        <td class=\"hidden-sm hidden-xs\" scope=\"col\">{{ customer.ZipCode }}</td>\n        <td class=\"hidden-sm hidden-xs\" scope=\"col\">{{ customer.Phone }}</td>\n        <td scope=\"col\">{{ customer.MobilePhone }}</td>\n        <td class=\"hidden-sm hidden-xs\" scope=\"col\">{{ customer.Email }}</td>\n        <td class=\"d-none d-sm-block\" scope=\"col\">{{ customer.Website }}</td>\n      </tr>\n    </tbody>\n  </table>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/insert/insert.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/insert/insert.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container mb-3\">\n  <div class=\"row ml-1 mr-1 mt-3 mb-5 text-light \">\n    <button\n      class=\"btn bg-secondary  btn-block col-md-3\"\n      [routerLink]=\"['../index']\"\n    >\n      back\n    </button>\n  </div>\n  <form>\n    <div class=\"mb-3\">\n      <label for=\"customer.CustomerName\">Customer name:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.CustomerName\"\n        name=\"customer.CustomerNamer\"\n        type=\"text\"\n        [(ngModel)]=\"customer.CustomerName\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.Address\">Address:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.Address\"\n        name=\"customer.Address\"\n        type=\"text\"\n        [(ngModel)]=\"customer.Address\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.Address\">City:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.City\"\n        name=\"customer.City\"\n        type=\"text\"\n        [(ngModel)]=\"customer.City\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.ZipCode\">ZipCode:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.ZipCode\"\n        name=\"customer.ZipCode\"\n        type=\"text\"\n        [(ngModel)]=\"customer.ZipCode\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.Phone\">Phone:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.Phone\"\n        name=\"customer.Phone\"\n        type=\"text\"\n        [(ngModel)]=\"customer.Phone\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.MobilePhone\">Mobile Phone:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.MobilePhone\"\n        name=\"customer.MobilePhone\"\n        type=\"text\"\n        [(ngModel)]=\"customer.MobilePhone\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.Email\">Email:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.Email\"\n        name=\"customer.Email\"\n        type=\"text\"\n        [(ngModel)]=\"customer.Email\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.Website\">Website:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.Website\"\n        name=\"customer.Website\"\n        type=\"text\"\n        [(ngModel)]=\"customer.Website\"\n      />\n    </div>\n    <div class=\"mb-3\">\n      <label for=\"customer.Vergi\">Vergi:</label>\n      <input\n        class=\"form-control\"\n        id=\"customer.Vergi\"\n        name=\"customer.Vergi\"\n        type=\"text\"\n        [(ngModel)]=\"customer.Vergi\"\n      />\n    </div>\n    <p class=\"bg-success p-3 mb-2\" *ngIf=\"showSuccess\">Successfully Saved!</p>\n    <div class=\"row\">\n      <div class=\"col-md-3 mb-3\">\n        <button\n          class=\"btn btn-secondary btn-lg btn-block\"\n          type=\"button\"\n          (click)=\"Cancel()\"\n        >\n          Cancel\n        </button>\n      </div>\n      <div class=\"col-md-3 mb-3\">\n        <button\n          class=\"btn btn-success btn-lg btn-block\"\n          type=\"button\"\n          value=\"Save\"\n          (click)=\"Save(customer)\"\n        >\n          Save\n        </button>\n      </div>\n    </div>\n  </form>\n</div>\n"

/***/ }),

/***/ "./src/app/Customer.ts":
/*!*****************************!*\
  !*** ./src/app/Customer.ts ***!
  \*****************************/
/*! exports provided: Customer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Customer", function() { return Customer; });
var Customer = /** @class */ (function () {
    function Customer() {
    }
    return Customer;
}());



/***/ }),

/***/ "./src/app/app.component.sass":
/*!************************************!*\
  !*** ./src/app/app.component.sass ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'customers';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.sass */ "./src/app/app.component.sass")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _filter_pipe__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./filter.pipe */ "./src/app/filter.pipe.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _edit_edit_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./edit/edit.component */ "./src/app/edit/edit.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _insert_insert_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./insert/insert.component */ "./src/app/insert/insert.component.ts");











var appRoutes = [
    { path: "index", component: _home_home_component__WEBPACK_IMPORTED_MODULE_9__["HomeComponent"] },
    { path: "edit", component: _edit_edit_component__WEBPACK_IMPORTED_MODULE_7__["EditComponent"] },
    { path: "insert", component: _insert_insert_component__WEBPACK_IMPORTED_MODULE_10__["InsertComponent"] },
    { path: "", redirectTo: "/index", pathMatch: "full" },
    { path: "**", component: _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _filter_pipe__WEBPACK_IMPORTED_MODULE_5__["FilterPipe"],
                _edit_edit_component__WEBPACK_IMPORTED_MODULE_7__["EditComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_9__["HomeComponent"],
                _insert_insert_component__WEBPACK_IMPORTED_MODULE_10__["InsertComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"].forRoot(appRoutes),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/edit/edit.component.sass":
/*!******************************************!*\
  !*** ./src/app/edit/edit.component.sass ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VkaXQvZWRpdC5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/edit/edit.component.ts":
/*!****************************************!*\
  !*** ./src/app/edit/edit.component.ts ***!
  \****************************************/
/*! exports provided: EditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditComponent", function() { return EditComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Customer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Customer */ "./src/app/Customer.ts");





var EditComponent = /** @class */ (function () {
    function EditComponent(http, route, router) {
        this.http = http;
        this.route = route;
        this.router = router;
        this.customer = new _Customer__WEBPACK_IMPORTED_MODULE_4__["Customer"]();
        this.showSuccess = false;
        this.showError = false;
    }
    EditComponent.prototype.ngOnInit = function () {
        var _this = this;
        var id = this.route.snapshot.queryParamMap.get("id");
        var customerURL = "/api/Customer/GetCustomer/" + id;
        this.http
            .get(customerURL)
            .toPromise()
            .then(function (res) {
            _this.customer = res;
        });
    };
    EditComponent.prototype.Save = function (customer) {
        var _this = this;
        this.showSuccess = false;
        var customerURL = "/api/Customer/Update";
        this.http
            .put(customerURL, customer)
            .toPromise()
            .then(function (res) {
            _this.showSuccess = true;
        });
    };
    EditComponent.prototype.Delete = function (customer) {
        var _this = this;
        this.showSuccess = false;
        var customerURL = "/api/Customer/Remove/" + customer.CustomerId;
        this.http
            .delete(customerURL)
            .toPromise()
            .then(function (res) {
            _this.showSuccess = true;
            _this.router.navigateByUrl("/index");
        });
    };
    EditComponent.prototype.Cancel = function (customer) {
        this.router.navigateByUrl("/index");
    };
    EditComponent.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    EditComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-edit",
            template: __webpack_require__(/*! raw-loader!./edit.component.html */ "./node_modules/raw-loader/index.js!./src/app/edit/edit.component.html"),
            styles: [__webpack_require__(/*! ./edit.component.sass */ "./src/app/edit/edit.component.sass")]
        })
    ], EditComponent);
    return EditComponent;
}());



/***/ }),

/***/ "./src/app/filter.pipe.ts":
/*!********************************!*\
  !*** ./src/app/filter.pipe.ts ***!
  \********************************/
/*! exports provided: FilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterPipe", function() { return FilterPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (items, searchText) {
        if (!items)
            return [];
        if (!searchText)
            return items;
        searchText = searchText.toString().toLowerCase();
        return items.filter(function (it) {
            return JSON.stringify(it)
                .toLowerCase()
                .includes(searchText);
        });
    };
    FilterPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: "filter"
        })
    ], FilterPipe);
    return FilterPipe;
}());



/***/ }),

/***/ "./src/app/home/home.component.sass":
/*!******************************************!*\
  !*** ./src/app/home/home.component.sass ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");




var HomeComponent = /** @class */ (function () {
    function HomeComponent(http, router) {
        this.http = http;
        this.router = router;
        this.customers = new Array();
        this.preventSingleClick = false;
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        var customersURL = "/api/Customer/";
        this.http
            .get(customersURL)
            .toPromise()
            .then(function (res) {
            _this.customers = res["Items"];
        });
    };
    HomeComponent.prototype.singleClick = function (event) {
        var _this = this;
        this.preventSingleClick = false;
        var delay = 200;
        this.timer = setTimeout(function () {
            if (!_this.preventSingleClick) {
                //Navigate on single click
                for (var i = 0; i < event.path[2].childElementCount; i++) {
                    event.path[2].children[i].className = "";
                }
                event.path[1].className = "bg-warning";
            }
        }, delay);
        var filterPallets = this.customers.filter(function (p) {
            return p.CustomerName.toLowerCase().includes(_this.searchText);
        });
    };
    HomeComponent.prototype.doubleClick = function (event) {
        this.preventSingleClick = true;
        clearTimeout(this.timer);
        //Navigate on double click
        this.router.navigate(["/edit/"], {
            queryParams: { id: event.currentTarget.children[0].innerText }
        });
    };
    HomeComponent.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-home",
            template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.sass */ "./src/app/home/home.component.sass")]
        })
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/insert/insert.component.sass":
/*!**********************************************!*\
  !*** ./src/app/insert/insert.component.sass ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2luc2VydC9pbnNlcnQuY29tcG9uZW50LnNhc3MifQ== */"

/***/ }),

/***/ "./src/app/insert/insert.component.ts":
/*!********************************************!*\
  !*** ./src/app/insert/insert.component.ts ***!
  \********************************************/
/*! exports provided: InsertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InsertComponent", function() { return InsertComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Customer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Customer */ "./src/app/Customer.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var InsertComponent = /** @class */ (function () {
    function InsertComponent(http, route, router) {
        this.http = http;
        this.route = route;
        this.router = router;
    }
    InsertComponent.prototype.ngOnInit = function () {
        this.customer = new _Customer__WEBPACK_IMPORTED_MODULE_2__["Customer"]();
    };
    InsertComponent.prototype.Save = function (customer) {
        var _this = this;
        this.showSuccess = false;
        var palletsURL = "/api/Customer/Insert";
        this.http
            .post(palletsURL, customer)
            .toPromise()
            .then(function (res) {
            _this.showSuccess = true;
        });
    };
    InsertComponent.prototype.Cancel = function () {
        this.router.navigateByUrl("/index");
    };
    InsertComponent.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
    ]; };
    InsertComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-insert",
            template: __webpack_require__(/*! raw-loader!./insert.component.html */ "./node_modules/raw-loader/index.js!./src/app/insert/insert.component.html"),
            styles: [__webpack_require__(/*! ./insert.component.sass */ "./src/app/insert/insert.component.sass")]
        })
    ], InsertComponent);
    return InsertComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/inventory/coderush/Views/Customer/customers/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map