﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace coderush.Migrations
{
    public partial class VendorChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VendorTypeId",
                table: "Vendor");

            migrationBuilder.RenameColumn(
                name: "State",
                table: "Vendor",
                newName: "Website");

            migrationBuilder.RenameColumn(
                name: "ContactPerson",
                table: "Vendor",
                newName: "Vergi");

            migrationBuilder.AddColumn<string>(
                name: "MobilePhone",
                table: "Vendor",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MobilePhone",
                table: "Vendor");

            migrationBuilder.RenameColumn(
                name: "Website",
                table: "Vendor",
                newName: "State");

            migrationBuilder.RenameColumn(
                name: "Vergi",
                table: "Vendor",
                newName: "ContactPerson");

            migrationBuilder.AddColumn<int>(
                name: "VendorTypeId",
                table: "Vendor",
                nullable: false,
                defaultValue: 0);
        }
    }
}
