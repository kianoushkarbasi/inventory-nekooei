﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace coderush.Migrations
{
    public partial class AddSellingFieldsForPallet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Sold",
                table: "Product",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<double>(
                name: "RemainWeight",
                table: "Pallet",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<bool>(
                name: "Sold",
                table: "Pallet",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sold",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "RemainWeight",
                table: "Pallet");

            migrationBuilder.DropColumn(
                name: "Sold",
                table: "Pallet");
        }
    }
}
