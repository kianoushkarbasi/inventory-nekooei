﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace coderush.Migrations
{
    public partial class CustomerRemoveType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerTypeId",
                table: "Customer");

            migrationBuilder.RenameColumn(
                name: "State",
                table: "Customer",
                newName: "Website");

            migrationBuilder.RenameColumn(
                name: "ContactPerson",
                table: "Customer",
                newName: "MobilePhone");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Website",
                table: "Customer",
                newName: "State");

            migrationBuilder.RenameColumn(
                name: "MobilePhone",
                table: "Customer",
                newName: "ContactPerson");

            migrationBuilder.AddColumn<int>(
                name: "CustomerTypeId",
                table: "Customer",
                nullable: false,
                defaultValue: 0);
        }
    }
}
