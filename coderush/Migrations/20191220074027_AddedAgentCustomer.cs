﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace coderush.Migrations
{
    public partial class AddedAgentCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customer_Agent_AgentId",
                table: "Customer");

            migrationBuilder.DropIndex(
                name: "IX_Customer_AgentId",
                table: "Customer");

            migrationBuilder.DropColumn(
                name: "AgentId",
                table: "Customer");

            migrationBuilder.CreateTable(
                name: "AgentCustomer",
                columns: table => new
                {
                    CustomerId = table.Column<int>(nullable: false),
                    AgentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentCustomer", x => new { x.CustomerId, x.AgentId });
                    table.ForeignKey(
                        name: "FK_AgentCustomer_Agent_AgentId",
                        column: x => x.AgentId,
                        principalTable: "Agent",
                        principalColumn: "AgentId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AgentCustomer_Customer_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customer",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AgentCustomer_AgentId",
                table: "AgentCustomer",
                column: "AgentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AgentCustomer");

            migrationBuilder.AddColumn<int>(
                name: "AgentId",
                table: "Customer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Customer_AgentId",
                table: "Customer",
                column: "AgentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Customer_Agent_AgentId",
                table: "Customer",
                column: "AgentId",
                principalTable: "Agent",
                principalColumn: "AgentId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
