﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace coderush.Migrations
{
    public partial class ModifiedBill : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BillTypeId",
                table: "Bill");

            migrationBuilder.DropColumn(
                name: "GoodsReceivedNoteId",
                table: "Bill");

            migrationBuilder.RenameColumn(
                name: "VendorDONumber",
                table: "Bill",
                newName: "Comment");

            migrationBuilder.AddColumn<double>(
                name: "Amount",
                table: "Bill",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "KDV",
                table: "Bill",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Paid",
                table: "Bill",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "VendorId",
                table: "Bill",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Bill_VendorId",
                table: "Bill",
                column: "VendorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bill_Vendor_VendorId",
                table: "Bill",
                column: "VendorId",
                principalTable: "Vendor",
                principalColumn: "VendorId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bill_Vendor_VendorId",
                table: "Bill");

            migrationBuilder.DropIndex(
                name: "IX_Bill_VendorId",
                table: "Bill");

            migrationBuilder.DropColumn(
                name: "Amount",
                table: "Bill");

            migrationBuilder.DropColumn(
                name: "KDV",
                table: "Bill");

            migrationBuilder.DropColumn(
                name: "Paid",
                table: "Bill");

            migrationBuilder.DropColumn(
                name: "VendorId",
                table: "Bill");

            migrationBuilder.RenameColumn(
                name: "Comment",
                table: "Bill",
                newName: "VendorDONumber");

            migrationBuilder.AddColumn<int>(
                name: "BillTypeId",
                table: "Bill",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "GoodsReceivedNoteId",
                table: "Bill",
                nullable: false,
                defaultValue: 0);
        }
    }
}
