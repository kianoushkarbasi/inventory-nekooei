﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace coderush.Migrations
{
    public partial class ReplacePalletIdWithPalletNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PalletId",
                table: "Product");

            migrationBuilder.AddColumn<string>(
                name: "PalletNumber",
                table: "Product",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PalletNumber",
                table: "Product");

            migrationBuilder.AddColumn<int>(
                name: "PalletId",
                table: "Product",
                nullable: false,
                defaultValue: 0);
        }
    }
}
