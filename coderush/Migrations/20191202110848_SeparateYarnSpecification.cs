﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace coderush.Migrations
{
    public partial class SeparateYarnSpecification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BobbinSize",
                table: "Pallet",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Color",
                table: "Pallet",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ColorCode",
                table: "Pallet",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Denier",
                table: "Pallet",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Filament",
                table: "Pallet",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Intermingle",
                table: "Pallet",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "Pallet",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "YarnType",
                table: "Pallet",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Discount",
                table: "Invoice",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BobbinSize",
                table: "Pallet");

            migrationBuilder.DropColumn(
                name: "Color",
                table: "Pallet");

            migrationBuilder.DropColumn(
                name: "ColorCode",
                table: "Pallet");

            migrationBuilder.DropColumn(
                name: "Denier",
                table: "Pallet");

            migrationBuilder.DropColumn(
                name: "Filament",
                table: "Pallet");

            migrationBuilder.DropColumn(
                name: "Intermingle",
                table: "Pallet");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "Pallet");

            migrationBuilder.DropColumn(
                name: "YarnType",
                table: "Pallet");

            migrationBuilder.DropColumn(
                name: "Discount",
                table: "Invoice");
        }
    }
}
