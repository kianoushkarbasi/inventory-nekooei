﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace coderush.Migrations
{
    public partial class RemoveListPalletsInvoice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pallet_Invoice_InvoiceId",
                table: "Pallet");

            migrationBuilder.DropIndex(
                name: "IX_Pallet_InvoiceId",
                table: "Pallet");

            migrationBuilder.DropColumn(
                name: "InvoiceId",
                table: "Pallet");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "InvoiceId",
                table: "Pallet",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Pallet_InvoiceId",
                table: "Pallet",
                column: "InvoiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Pallet_Invoice_InvoiceId",
                table: "Pallet",
                column: "InvoiceId",
                principalTable: "Invoice",
                principalColumn: "InvoiceId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
