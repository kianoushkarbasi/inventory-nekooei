﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using coderush.Models;

namespace coderush.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

             builder.Entity<Invoice>();
             builder.Entity<AgentCustomer>().HasKey(sc => new { sc.CustomerId, sc.AgentId });
             builder.Entity<InvoicePallet>().HasKey(sc => new { sc.InvoiceId, sc.PalletId });
             builder.Entity<InvoiceBox>().HasKey(sc => new { sc.InvoiceId, sc.BoxId });
        }

        public DbSet<coderush.Models.ApplicationUser> ApplicationUser { get; set; }

        public DbSet<coderush.Models.Bill> Bill { get; set; }
        public DbSet<coderush.Models.Agent> Agent { get; set; }

        public DbSet<coderush.Models.BillType> BillType { get; set; }

        public DbSet<coderush.Models.Branch> Branch { get; set; }

        public DbSet<coderush.Models.CashBank> CashBank { get; set; }

        public DbSet<coderush.Models.Currency> Currency { get; set; }

        public DbSet<coderush.Models.Customer> Customer { get; set; }

        public DbSet<coderush.Models.CustomerType> CustomerType { get; set; }

        public DbSet<coderush.Models.GoodsReceivedNote> GoodsReceivedNote { get; set; }

        public DbSet<coderush.Models.Invoice> Invoice { get; set; }

        public DbSet<coderush.Models.InvoiceType> InvoiceType { get; set; }

        public DbSet<coderush.Models.NumberSequence> NumberSequence { get; set; }

        public DbSet<coderush.Models.PaymentReceive> PaymentReceive { get; set; }

        public DbSet<coderush.Models.PaymentType> PaymentType { get; set; }

        public DbSet<coderush.Models.PaymentVoucher> PaymentVoucher { get; set; }
        
        public DbSet<coderush.Models.Pallet> Pallet { get; set; }
        public DbSet<coderush.Models.Box> Box { get; set; }

        
        public DbSet<coderush.Models.PurchaseType> PurchaseType { get; set; }

        
        public DbSet<coderush.Models.Vendor> Vendor { get; set; }

        public DbSet<coderush.Models.VendorType> VendorType { get; set; }

        public DbSet<coderush.Models.Warehouse> Warehouse { get; set; }

        public DbSet<coderush.Models.UserProfile> UserProfile { get; set; }
        public DbSet<coderush.Models.AgentCustomer> AgentCustomer { get; set; }
        public DbSet<coderush.Models.InvoicePallet> InvoicePallet { get; set; }
        public DbSet<coderush.Models.InvoiceBox> InvoiceBox { get; set; }
    }
}
