﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace coderush.Controllers
{
    [Authorize(Roles = Pages.MainMenu.Pallet.RoleName)]
    public class PalletController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
