﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using coderush.Data;
using coderush.Models;
using coderush.Services;
using Microsoft.AspNetCore.Authorization;

namespace coderush.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Invoice")]
    public class InvoiceController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INumberSequence _numberSequence;

        public InvoiceController(ApplicationDbContext context,
                        INumberSequence numberSequence)
        {
            _context = context;
            _numberSequence = numberSequence;
        }

        // GET: api/Invoice
        [HttpGet]
        public async Task<IActionResult> GetInvoice()
        {
            List<Invoice> Items = await _context.Invoice
            .Include(p=>p.InvoicePallets).ThenInclude(w=>w.Pallet)
            .Include(p=>p.Customer).ToListAsync();
            int Count = Items.Count();
            return Ok(new { Items, Count });
        }

         // GET: api/Invoice
        [HttpGet("GetInvoice/{id}")]
        public  IActionResult GetInvoice(int id)
        {
            Invoice Item =  _context.Invoice.Include(p=>p.Customer)
            .Include(p=>p.InvoiceBoxes).ThenInclude(p=>p.Box)
            .Include(p=>p.InvoicePallets).ThenInclude(p=>p.Pallet)
                .FirstOrDefault(p=>p.InvoiceId == id);            
            return Ok(Item);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetNotPaidYet()
        {
            List<Invoice> invoices = new List<Invoice>();
            try
            {
                List<PaymentReceive> receives = new List<PaymentReceive>();
                receives = await _context.PaymentReceive.ToListAsync();
                List<int> ids = new List<int>();

                foreach (var item in receives)
                {
                    ids.Add(item.InvoiceId);
                }

                invoices = await _context.Invoice
                    .Where(x => !ids.Contains(x.InvoiceId))
                    .ToListAsync();
            }
            catch (Exception)
            {

                throw;
            }
            return Ok(invoices);
        }

        [HttpPost("[action]")]
        public IActionResult Insert([FromBody]Invoice invoice)
        {   
            invoice.Customer = _context.Customer.FirstOrDefault(p=>p.CustomerId == invoice.Customer.CustomerId);
            foreach(var invoicePallet in invoice.InvoicePallets){
                var pallet =_context.Pallet.FirstOrDefault(p=>p.PalletId == invoicePallet.PalletId);
                pallet.RemainWeight -= invoicePallet.Weight;
                if(pallet.RemainWeight <= 0)
                   pallet.Sold = true;
            }
            
            foreach(var invoiceBox in invoice.InvoiceBoxes){
                var box =_context.Box.FirstOrDefault(p=>p.BoxId == invoiceBox.BoxId);
                box.RemainWeight -= invoiceBox.Weight;
                if(box.RemainWeight <= 0)
                   box.Sold = true;
            }
            _context.Invoice.Add(invoice);            
            _context.SaveChanges();
            return Ok(invoice);
        }

        [HttpPost("[action]")]
        public IActionResult Update([FromBody]Invoice invoice)
        {   
            var newCustomer = _context.Customer
            .FirstOrDefault(p=>p.CustomerId == invoice.Customer.CustomerId);
            var oldInvoice =_context.Invoice.Include(p=>p.InvoicePallets).Include(p=>p.InvoiceBoxes).FirstOrDefault(p=>p.InvoiceId == invoice.InvoiceId);                                    
            _context.InvoicePallet.RemoveRange(oldInvoice.InvoicePallets);
            _context.InvoiceBox.RemoveRange(oldInvoice.InvoiceBoxes);

            foreach(var invoicePallet in oldInvoice.InvoicePallets){                
                var pallet = _context.Pallet.FirstOrDefault(p=>p.PalletId == invoicePallet.PalletId);
                pallet.RemainWeight += invoicePallet.Weight;                
                pallet.Sold = false;
                _context.Pallet.Update(pallet);
            }

            foreach(var invoiceBox in oldInvoice.InvoiceBoxes){                
                var box = _context.Box.FirstOrDefault(p=>p.BoxId == invoiceBox.BoxId);
                box.RemainWeight += invoiceBox.Weight;                
                box.Sold = false;
                _context.Box.Update(box);
            }
            _context.SaveChanges();         

            foreach(var invoicePallet in invoice.InvoicePallets){
                var pallet =_context.Pallet.FirstOrDefault(p=>p.PalletId == invoicePallet.PalletId);
                pallet.RemainWeight -= invoicePallet.Weight;
                if(pallet.RemainWeight <= 0)
                    pallet.Sold = true;
            }

            foreach(var invoiceBox in invoice.InvoiceBoxes){
                var box =_context.Box.FirstOrDefault(p=>p.BoxId == invoiceBox.BoxId);
                box.RemainWeight -= invoiceBox.Weight;
                if(box.RemainWeight <= 0)
                    box.Sold = true;
            }

            oldInvoice.Customer = newCustomer;
            oldInvoice.ExchangeRate = invoice.ExchangeRate;
            oldInvoice.InvoiceDate = invoice.InvoiceDate.ToLocalTime();
            oldInvoice.InvoiceDueDate = invoice.InvoiceDueDate.ToLocalTime();
            oldInvoice.Paid = invoice.Paid;
            oldInvoice.TotalUsd = invoice.TotalUsd;
            oldInvoice.Kdv = invoice.Kdv;
            oldInvoice.StateInvoiceNumber = invoice.StateInvoiceNumber;
            oldInvoice.Tax = invoice.Tax;            
            oldInvoice.InvoicePallets = invoice.InvoicePallets;
            oldInvoice.InvoiceBoxes = invoice.InvoiceBoxes;
            oldInvoice.Discount = invoice.Discount;
            oldInvoice.Currency = invoice.Currency;
            
            _context.Invoice.Update(oldInvoice);
            
            _context.SaveChanges();
            return Ok(invoice);
        }

        [HttpDelete("[action]")]
        public IActionResult Delete(int invoiceId)
        {
            Invoice invoiceOld = _context.Invoice
                .Include(p=>p.InvoicePallets)
                .Where(x => x.InvoiceId == invoiceId)
                .FirstOrDefault();
            
            foreach(var invoicePallet in invoiceOld.InvoicePallets){                
                var pallet = _context.Pallet.FirstOrDefault(p=>p.PalletId == invoicePallet.PalletId);
                pallet.RemainWeight += invoicePallet.Weight;                
                _context.Pallet.Update(pallet);
            }

            foreach(var invoiceBox in invoiceOld.InvoiceBoxes){                
                var box = _context.Box.FirstOrDefault(p=>p.BoxId == invoiceBox.BoxId);
                box.RemainWeight += invoiceBox.Weight;                
                _context.Box.Update(box);
            }

            _context.Invoice.Remove(invoiceOld);
            _context.SaveChanges();
            return Ok(invoiceOld);

        }
    }
}