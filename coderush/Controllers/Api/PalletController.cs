﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using coderush.Data;
using coderush.Models;
using coderush.Models.SyncfusionViewModels;
using Microsoft.AspNetCore.Authorization;

namespace coderush.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Pallet")]
    public class PalletController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PalletController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Pallet
        [HttpGet]
        public async Task<IActionResult> GetPallets()
        {  
                List<Pallet> Items = await _context.Pallet.ToListAsync();
                int Count = Items.Count();
                return Ok(new { Items, Count });
           
        } 

          [HttpGet("GetAvailablePallets")]
        public async Task<IActionResult> GetAvailablePallets()
        {  
                List<Pallet> Items = await _context.Pallet.Where(p=>p.Sold == false).ToListAsync();
                int Count = Items.Count();
                return Ok(new { Items, Count });
           
        } 
        [HttpGet("GetPallet/{id}")]
        public IActionResult GetPallet(int id)
        {  
            Pallet pallet = _context.Pallet.FirstOrDefault(p=>p.PalletId == id);                
            return Ok(pallet);           
        } 

        [HttpPost("[action]")]
        public IActionResult Insert([FromBody]Pallet pallet)
        {            
            _context.Pallet.Add(pallet);
            _context.SaveChanges();
            return Ok(pallet);
        }

        [HttpPut("[action]")]
        public IActionResult Update([FromBody]Pallet pallet)
        {        
            _context.Pallet.Update((Pallet)pallet);
            _context.SaveChanges();
            return Ok(pallet);
        }
        [HttpGet("[action]")]
        public IActionResult Print(int id,string customerName)
        {      
            var pallet = _context.Pallet.FirstOrDefault(p=>p.PalletId == id);
            var spltDetails = pallet.PalletName.Split(' ');
            
            var color = spltDetails.Length>2 ? spltDetails[2] : " ";
            var colorCode = spltDetails.Length>3 ? spltDetails[3] : " ";
            var denier = spltDetails.Length>4 ? spltDetails[4] : " ";
            var type = spltDetails.Length>1 ? spltDetails[1] : " ";

            string command = $"python htmltopdf.py {customerName} {color} {colorCode} {denier} {type} {pallet.Barcode} {pallet.Weight} {pallet.PalletId}";
            string result = "";
            

            using (System.Diagnostics.Process proc = new System.Diagnostics.Process())
            {
                proc.StartInfo.FileName = "/bin/bash";
                proc.StartInfo.Arguments = "-c \" " + command + " \"";
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;
                proc.Start();

                result += proc.StandardOutput.ReadToEnd();
                result += proc.StandardError.ReadToEnd();

                proc.WaitForExit();
            }     
            return Ok();
        }

        [HttpDelete("Remove/{id}")]
        public IActionResult Remove(int id)
        {            
            var pallet = _context.Pallet
                .Where(x => x.PalletId == id)
                .FirstOrDefault();
            _context.Pallet.Remove(pallet);
            _context.SaveChanges();
            return Ok(pallet);
        }
    }
}