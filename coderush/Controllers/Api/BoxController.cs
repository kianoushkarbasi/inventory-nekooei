﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using coderush.Data;
using coderush.Models;
using Microsoft.AspNetCore.Authorization;

namespace coderush.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Box")]
    public class BoxController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BoxController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Box
        [HttpGet]
        public async Task<IActionResult> Getboxs()
        {  
                List<Box> Items = await _context.Box.ToListAsync();
                int Count = Items.Count();
                return Ok(new { Items, Count });
           
        } 

          [HttpGet("GetAvailableboxes")]
        public async Task<IActionResult> GetAvailableboxs()
        {  
                List<Box> Items = await _context.Box.Where(p=>p.Sold == false).ToListAsync();
                int Count = Items.Count();
                return Ok(new { Items, Count });
           
        } 
        [HttpGet("Getbox/{id}")]
        public IActionResult Getbox(int id)
        {  
            Box box = _context.Box.FirstOrDefault(p=>p.BoxId == id);                
            return Ok(box);           
        } 

        [HttpPost("[action]")]
        public IActionResult Insert([FromBody]Box box)
        {            
            _context.Box.Add(box);
            _context.SaveChanges();
            return Ok(box);
        }

        [HttpPut("[action]")]
        public IActionResult Update([FromBody]Box box)
        {        
            _context.Box.Update((Box)box);
            _context.SaveChanges();
            return Ok(box);
        }
    

        [HttpDelete("Remove/{id}")]
        public IActionResult Remove(int id)
        {            
            var box = _context.Box
                .Where(x => x.BoxId == id)
                .FirstOrDefault();
            _context.Box.Remove(box);
            _context.SaveChanges();
            return Ok(box);
        }
    }
}