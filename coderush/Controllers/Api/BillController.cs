﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using coderush.Data;
using coderush.Models;
using coderush.Services;
using Microsoft.AspNetCore.Authorization;

namespace coderush.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Bill")]
    public class BillController : Controller
    {
          private readonly ApplicationDbContext _context;
        private readonly INumberSequence _numberSequence;

        public BillController(ApplicationDbContext context,
                        INumberSequence numberSequence)
        {
            _context = context;
            _numberSequence = numberSequence;
        }

        // GET: api/Bill
        [HttpGet]
        public async Task<IActionResult> GetBill()
        {
            List<Bill> Items = await _context.Bill.Include(p=>p.Vendor).ToListAsync();
            int Count = Items.Count();
            return Ok(new { Items, Count });
        }

         // GET: api/Bill
        [HttpGet("GetBill/{id}")]
        public  IActionResult GetBill(int id)
        {
            Bill Item =  _context.Bill.Include(p=>p.Vendor)
                .FirstOrDefault(p=>p.BillId == id);            
            return Ok(Item);
        }

        [HttpGet("[action]")]
        public IActionResult GetNotPaidYet()
        {
            List<Bill> Bills = new List<Bill>();            
            return Ok(Bills);
        }

        [HttpPost("[action]")]
        public IActionResult Insert([FromBody]Bill bill)
        {   
            bill.Vendor = _context.Vendor.FirstOrDefault(p=>p.VendorId == bill.Vendor.VendorId);
            var existingPallets = new List<Pallet>();
            _context.Bill.Add(bill);            
            _context.SaveChanges();
            return Ok(bill);
        }

        [HttpPost("[action]")]
        public IActionResult Update([FromBody]Bill bill)
        {   
            var newVendor = _context.Vendor
            .FirstOrDefault(p=>p.VendorId == bill.Vendor.VendorId);
            var oldBill =_context.Bill.FirstOrDefault(p=>p.BillId == bill.BillId);            
            
          
            oldBill.Vendor = newVendor;
            oldBill.Amount = bill.Amount;
            oldBill.BillDate = bill.BillDate;
            oldBill.BillDueDate = bill.BillDueDate;
            oldBill.BillName = bill.BillName;
            oldBill.Comment = bill.Comment;
            oldBill.Paid = bill.Paid;
            oldBill.KDV = bill.KDV;
            oldBill.UsdAmount = bill.UsdAmount;
            oldBill.VendorInvoiceNumber = bill.VendorInvoiceNumber;
            
            _context.Bill.Update(oldBill);            
            _context.SaveChanges();

            return Ok(bill);
        }

        [HttpDelete("[action]")]
        public IActionResult Delete(int BillId)
        {
            Bill BillOld = _context.Bill
                .Where(x => x.BillId == BillId)
                .FirstOrDefault();
            _context.Bill.Remove(BillOld);
            _context.SaveChanges();

            return Ok(BillOld);

        }
    }
}