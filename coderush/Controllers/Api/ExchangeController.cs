using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Xml;

namespace coderush.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Exchange")]
    public class ExchangeController : Controller
    {
           // GET: api/Invoice
        [HttpGet]
        public IActionResult Get()
        {
            var rate = 5.7;
            var URLString = "https://www.tcmb.gov.tr/kurlar/today.xml";
            var reader = new XmlTextReader (URLString);
            var cnt = 0;
            
            while (reader.Read()) 
            {
                switch (reader.NodeType) 
                {
                    case XmlNodeType.Element: 
                        break;

                    case XmlNodeType.Text:                         
                        if(cnt==6)
                            double.TryParse(reader.Value,out rate);

                        cnt++;
                        break;

                    case XmlNodeType. EndElement: //Display the end of the element.
                        break;
                }
            }
            return Ok(rate);
        }

    }
}