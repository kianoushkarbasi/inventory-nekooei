﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using coderush.Data;
using coderush.Models;
using Microsoft.AspNetCore.Authorization;

namespace coderush.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Vendor")]
    public class VendorController : Controller
    {
       private readonly ApplicationDbContext _context;

        public VendorController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Vendor
        [HttpGet]
        public async Task<IActionResult> GetVendor()
        {
            List<Vendor> Items = await _context.Vendor.ToListAsync();
            int Count = Items.Count();
            return Ok(new { Items, Count });
        }

        [HttpGet("GetVendor/{id}")]
        public IActionResult GetVendor(int id)
        {  
            var vendor = _context.Vendor.FirstOrDefault(p=>p.VendorId == id);                
            return Ok(vendor);           
        } 


        [HttpPost("[action]")]
        public IActionResult Insert([FromBody]Vendor vendor)
        {            
            _context.Vendor.Add(vendor);
            _context.SaveChanges();
            return Ok(vendor);
        }

        [HttpPut("[action]")]
        public IActionResult Update([FromBody]Vendor vendor)
        {            
            _context.Vendor.Update(vendor);
            _context.SaveChanges();
            return Ok(vendor);
        }

        [HttpDelete("Remove/{id}")]
        public IActionResult Remove(int id)
        {            
            var vendor = _context.Vendor
                .Where(x => x.VendorId == id)
                .FirstOrDefault();
            _context.Vendor.Remove(vendor);
            _context.SaveChanges();
            return Ok(vendor);
        }
    }
}