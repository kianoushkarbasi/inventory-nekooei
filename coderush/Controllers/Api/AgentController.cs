﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using coderush.Data;
using coderush.Models;
using Microsoft.AspNetCore.Authorization;

namespace coderush.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Agent")]
    public class AgentController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AgentController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Agents
        [HttpGet]
        public async Task<IActionResult> GetAgents()
        {
            List<Agent> Items = await _context.Agent.ToListAsync();
            int Count = Items.Count();
            return Ok(new { Items, Count });
        }

        [HttpGet("GetAgent/{id}")]
        public IActionResult GetAgent(int id)
        {  
            var agent = _context.Agent.Include(p=>p.Customers).ThenInclude(w=>w.Customer).FirstOrDefault(p=>p.AgentId == id);                
            return Ok(agent);           
        } 


        [HttpPost("[action]")]
        public IActionResult Insert([FromBody]Agent agent)
        {            
            _context.Agent.Add(agent);
            _context.SaveChanges();
            return Ok(agent);
        }

        [HttpPut("[action]")]
        public IActionResult Update([FromBody]Agent agent)
        {            
            _context.Agent.Update(agent);
            _context.SaveChanges();
            return Ok(agent);
        }

        [HttpDelete("Remove/{id}")]
        public IActionResult Remove(int id)
        {            
            var agent = _context.Agent
                .Where(x => x.AgentId == id)
                .FirstOrDefault();
            _context.Agent.Remove(agent);
            _context.SaveChanges();
            return Ok(agent);            
        }
    }
}