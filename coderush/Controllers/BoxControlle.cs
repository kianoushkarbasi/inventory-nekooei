﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace coderush.Controllers
{
    [Authorize(Roles = Pages.MainMenu.Box.RoleName)]
    public class BoxController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
