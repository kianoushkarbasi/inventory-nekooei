import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { Box } from "../Box";
import { Pallet } from '../../../../../Pallet/pallet/src/app/pallet';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-insert",
  templateUrl: "./insert.component.html",
  styleUrls: ["./insert.component.sass"]
})
export class InsertComponent implements OnInit {
  box = new Box();
  showSuccess = false;
  showError = false;
  pallets = new Array<Pallet>();
  PalletsControl = new FormControl();
  filteredOptions: Observable<Pallet[]>;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    var palletUrl = "/api/Pallet/GetAvailablePallets";
    this.http.get<any>(palletUrl)
    .subscribe(p => {
      this.pallets = p.Items;
      this.filteredOptions = this.PalletsControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      ); 
    });

  }

  Save(box: Box) {
    this.showSuccess = false;
    var boxURL = "/api/Box/Insert";
    this.http
      .post<Box>(boxURL, box)
      .toPromise()
      .then(res => {
        this.showSuccess = true;
      });
  }

  private _filter(value: string): Pallet[] {
    const filterValue = value.toString().toLowerCase();

    return this.pallets.filter(option => option.PalletNumber.toString().includes(filterValue)||
    option.YarnType.includes(filterValue) ||
    option.Denier.toString().includes(filterValue)||
    option.Filament.toString().includes(filterValue)  );
  }

  Add(newPalletId:number){
    this.box.PalletId = newPalletId;
  }

  Cancel() {
    this.router.navigateByUrl("/index");
  }
}
