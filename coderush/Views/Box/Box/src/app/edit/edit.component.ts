import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { Pallet } from '../../../../../Pallet/pallet/src/app/pallet';
import { Box } from "../Box";
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { FormControl } from "@angular/forms";


@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.sass"]
})
export class EditComponent implements OnInit {
  box = new Box();
  showSuccess = false;
  showError = false;
  pallets = new Array<Pallet>();
  PalletsControl = new FormControl();
  filteredOptions: Observable<Pallet[]>;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    var id = this.route.snapshot.queryParamMap.get("id");
    var boxsURL = "/api/box/Getbox/" + id;
    this.http
      .get<Box>(boxsURL)
      .toPromise()
      .then(res => {
        this.box = res;
      });
      var palletUrl = "/api/Pallet/GetAvailablePallets";
      this.http.get<any>(palletUrl)
      .subscribe(p => {
        this.pallets = p.Items;
        this.filteredOptions = this.PalletsControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter(value))
        ); 
      });
  }

  Save(box: Box) {
    this.showSuccess = false;
    var boxsURL = "/api/box/Update";
    this.http
      .put<Box>(boxsURL, box)
      .toPromise()
      .then(res => {
        this.showSuccess = true;
      });
  }
  private _filter(value: string): Pallet[] {
    const filterValue = value.toString().toLowerCase();

    return this.pallets.filter(option => option.PalletNumber.toString().includes(filterValue)||
    option.YarnType.includes(filterValue) ||
    option.Denier.toString().includes(filterValue)||
    option.Filament.toString().includes(filterValue)  );
  }

  Add(newPalletId:number){
    this.box.PalletId = newPalletId;
  }



  Delete() {
    this.showSuccess = false;
    var boxsURL = "/api/box/Remove/" + this.box.BoxId;
    this.http
      .delete<Box>(boxsURL)
      .toPromise()
      .then(res => {
        this.showSuccess = true;
        this.router.navigateByUrl("/index");
      });
  }
  Cancel(box: Box) {
    this.router.navigateByUrl("/index");
  }
}
