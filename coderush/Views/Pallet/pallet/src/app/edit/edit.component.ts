import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { Pallet } from "../pallet";

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.sass"]
})
export class EditComponent implements OnInit {
  pallet = new Pallet();
  showSuccess = false;
  showError = false;
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    var id = this.route.snapshot.queryParamMap.get("id");
    var palletsURL = "/api/Pallet/GetPallet/" + id;
    this.http
      .get<Pallet>(palletsURL)
      .toPromise()
      .then(res => {
        this.pallet = res;
      });
  }

  Save(pallet: Pallet) {
    this.showSuccess = false;
    var palletsURL = "/api/Pallet/Update";
    this.http
      .put<Pallet>(palletsURL, pallet)
      .toPromise()
      .then(res => {
        this.showSuccess = true;
      });
  }

  Delete(pallet: Pallet) {
    this.showSuccess = false;
    var palletsURL = "/api/Pallet/Remove/" + pallet.PalletId;
    this.http
      .delete<Pallet>(palletsURL)
      .toPromise()
      .then(res => {
        this.showSuccess = true;
        this.router.navigateByUrl("/index");
      });
  }
  Cancel(pallet: Pallet) {
    this.router.navigateByUrl("/index");
  }
}
