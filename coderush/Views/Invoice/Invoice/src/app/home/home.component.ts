import { Component, OnInit } from "@angular/core";
import { Invoice } from "../Invoice";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.sass"]
})
export class HomeComponent implements OnInit {
  invoices = new Array<Invoice>();
  preventSingleClick = false;
  timer: any;
  delay: Number;
  searchText: string;
  sortUp = new Array<boolean>();  

  constructor(private http: HttpClient, private router: Router) {}

  ngOnInit() {
    var invoiceURL = "/api/Invoice/";
    this.http
      .get<Invoice[]>(invoiceURL)
      .toPromise()
      .then(res => {
        this.invoices = res["Items"];
      });
  }

  doubleClick(event) {
    this.preventSingleClick = true;
    clearTimeout(this.timer);
    //Navigate on double click
    this.router.navigate(["/edit/"], {
      queryParams: { id: event.currentTarget.children[0].innerText }
    });
  }

  getColor(invoice:Invoice){
    var lira = invoice.TotalUsd * invoice.ExchangeRate;
    var isLira = false;

    var diffLira = invoice.Paid - lira;
    var diffUsd = invoice.Paid - invoice.TotalUsd;

    if(invoice.Currency === 'Lira' )
      isLira = true;

    if(isLira===false){
      if(diffUsd < -1)
        return "red";  
      if(diffUsd > 1)    
        return "green";    
    }else{
      if(diffLira < -1)
        return "red";  
      if(diffLira > 1)
        return "green";
    }    
    return "";
  }

  getDifference(invoice){
    var lira = invoice.TotalUsd * invoice.ExchangeRate;    

    var diffLira = invoice.Paid - lira;
    var diffUsd = invoice.Paid - invoice.TotalUsd;

    if(invoice.Currency === 'Lira' )
      return diffLira;
    else
      return diffUsd;

  }

  Sort(sortHeader:string){        
    switch(sortHeader){
      case 'InvoiceId':
      {
        this.sortUp[0] = !this.sortUp[0];
        if(this.sortUp[0]){
          this.invoices.sort((a,b) => a.InvoiceId - b.InvoiceId);
        }else{
          this.invoices.sort((a,b) => b.InvoiceId - a.InvoiceId);
        }
        break;
      }
      case 'CustomerName':
      {
          this.sortUp[1] = !this.sortUp[1];
          if(this.sortUp[1]){
            this.invoices.sort((a,b) => a.Customer.CustomerName.toLocaleLowerCase().localeCompare(b.Customer.CustomerName.toLocaleLowerCase()));
          }else{
            this.invoices.sort((a,b) => b.Customer.CustomerName.toLocaleLowerCase().localeCompare(a.Customer.CustomerName.toLocaleLowerCase()));
          }          
          break;
      }
      
      case 'InvoiceDate':
      {
          this.sortUp[2] = !this.sortUp[2];
          if(this.sortUp[2]){
            this.invoices.sort((a,b) => a.InvoiceDate.localeCompare(b.InvoiceDate));
          }else{
            this.invoices.sort((a,b) => b.InvoiceDate.localeCompare(a.InvoiceDate));
          }
          break;
      }
      case 'TotalUsd':
      {
          this.sortUp[4] = !this.sortUp[4];
          if(this.sortUp[4]){
            this.invoices.sort((a,b) =>a.TotalUsd - b.TotalUsd);
          }else{
             this.invoices.sort((a,b) =>b.TotalUsd - a.TotalUsd);
          }
          break;
       }
       case 'Paid':
        {
            this.sortUp[5] = !this.sortUp[5];
            if(this.sortUp[5]){
              this.invoices.sort((a,b) =>a.Paid - b.Paid);
            }else{
               this.invoices.sort((a,b) =>b.Paid - a.Paid);
            }
            break;
         }
      }
    }
}
