import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { Invoice } from "../Invoice";
import { FormControl } from "@angular/forms";
import { Customer } from "../../../../../Customer/customers/src/app/Customer";
import { Pallet } from "../../../../../Pallet/pallet/src/app/pallet";
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { InvoicePallet } from '../InvoicePallet';
import { Box } from '../../../../../Box/Box/src/app/Box';
import { InvoiceBox } from '../InvoiceBox';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  pallet = new Invoice();
  showSuccess = false;
  showError = false;
  CustomerControl = new FormControl();
  PalletsControl = new FormControl();
  BoxesControl = new FormControl();
  invoice = new Invoice();
  customers = new Array<Customer>();
  pallets = new Array<Pallet>();
  boxes = new Array<Box>();
  newPallet = new Pallet();
  newPalletId:number;
  invoicePallets = new Array<InvoicePallet>();  
  invoiceBoxes = new Array<InvoiceBox>();  
  ExchangeRate:number;
  TotalUsd:number;
  TotalLira:number;
  Tax:number;
  InvoiceReportUrl:string;
  ExitReportUrl:string;
  filteredOptions: Observable<Pallet[]>;
  boxFilteredOptions: Observable<Box[]>;
  customerFilteredOptions: Observable<Customer[]>;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.invoice.Customer = new Customer();    
    this.invoice.Kdv = 8;
    this.TotalUsd = 0;
    this.TotalLira = 0;
    this.ExchangeRate = 5.7;
  }

  ngOnInit() {

    var customerUrl = "/api/Customer/";
    var palletUrl = "/api/Pallet/GetAvailablePallets";
    var boxUrl = "/api/Box/GetAvailableBoxes";
    var invoiceUrl = "/api/Invoice/GetInvoice/";    
    var id = this.route.snapshot.queryParamMap.get("id");

    this.http.get<any>(customerUrl)
    .subscribe(p => {
      this.customers = p.Items;
      this.customerFilteredOptions = this.CustomerControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._customerFilter(value))
      );

    });

    this.http.get<any>(palletUrl)
    .subscribe(p => {
      this.pallets = p.Items;
      this.filteredOptions = this.PalletsControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    });
    this.http.get<any>(boxUrl)
    .subscribe(p => {
      this.boxes = p.Items;
      this.boxFilteredOptions = this.BoxesControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._boxFilter(value))
      ); 
    });

    this.http.get<any>(invoiceUrl +  id)
    .subscribe(p => {
      this.invoice = p;
      for(var i=0;i<p.InvoicePallets.length;i++){
         this.invoicePallets.push(p.InvoicePallets[i]); 
      }
      for(var i=0;i<p.InvoiceBoxes.length;i++){
        this.invoiceBoxes.push(p.InvoiceBoxes[i]); 
     }
      this.ExchangeRate = this.invoice.ExchangeRate;
      this.TotalUsd = this.invoice.TotalUsd;
      this.Tax = this.invoice.Tax;        
      this.Calculate();
    });
    
    this.InvoiceReportUrl = "/InvoiceReport/print/?id=" + id;
    this.ExitReportUrl = "/WarehouseTakeOutReport/print/?invoiceId=" + id;

  }

  Add(newPalletId:number){
    var selectedPallet = this.pallets.find( p=>p.PalletId == newPalletId);
    var newInvoicePallet = new InvoicePallet();
    newInvoicePallet.PalletId = selectedPallet.PalletId;
    newInvoicePallet.Pallet = selectedPallet;
    newInvoicePallet.InvoiceId = this.invoice.InvoiceId;
    newInvoicePallet.Invoice = this.invoice;
    newInvoicePallet.Weight = selectedPallet.RemainWeight;
    this.invoicePallets.push(newInvoicePallet);
    this.Calculate();    
  }

  private _filter(value: string): Pallet[] {
    const filterValue = value.toString().toLowerCase();

    return this.pallets.filter(
      option => option.PalletNumber.toString().includes(filterValue)||
      option.YarnType.includes(filterValue) ||
      option.Denier.toString().includes(filterValue)||
      option.Filament.toString().includes(filterValue));
  }
  private _boxFilter(value: string): Box[] {
    var filterValue = value.toString().toLowerCase();
    if(!value)
      filterValue = "";

    return this.boxes.filter(option => option.Barcode.toString().includes(filterValue)||
    option.YarnType.includes(filterValue) ||
    option.Denier.toString().includes(filterValue)||
    option.Filament.toString().includes(filterValue)  );
  }


  private _customerFilter(value: string): Customer[] {
    const filterValue = value.toString().toLowerCase();
    return this.customers.filter(option => option.CustomerName.toLocaleLowerCase().includes(filterValue));
  }

  Delete(){        
    this.showSuccess = false;
    var invoiceUrl = "/api/Invoice/Delete/?invoiceId=" + this.invoice.InvoiceId;
    this.http
      .delete(invoiceUrl)
      .toPromise()
      .then(res => {
        this.showSuccess = true;
        this.router.navigateByUrl("/home");
      });
  }

  Download(){
    var invoiceUrl = "/InvoiceReport/index/?invoiceId=" + this.invoice.InvoiceId;
    this.http
      .get(invoiceUrl)
      .toPromise()
      .then();
  }

  RemoveItem(selectedPallet:InvoicePallet)
  {    
    const index: number = this.invoicePallets.indexOf(selectedPallet);
    if (index !== -1) {
        this.invoicePallets.splice(index, 1);
    }           
    this.Calculate();
  }

  AddBox(newBoxId:number){
    var selectedBox = this.boxes.find( p=>p.BoxId == newBoxId);
    var newInvoiceBox = new InvoiceBox();
    newInvoiceBox.BoxId = selectedBox.BoxId;
    newInvoiceBox.Box = selectedBox;
    newInvoiceBox.InvoiceId = this.invoice.InvoiceId;
    newInvoiceBox.Invoice = this.invoice;
    newInvoiceBox.Weight = selectedBox.RemainWeight;
    this.invoiceBoxes.push(newInvoiceBox);
    this.Calculate();    
  }

  RemoveBoxItem(selectedBox:InvoiceBox)
  {    
    const index: number = this.invoiceBoxes.indexOf(selectedBox);
    if (index !== -1) {
        this.invoiceBoxes.splice(index, 1);
    }           
    this.Calculate();
  }

  Calculate(){
    this.TotalUsd = 0;
    
    for(var i=0;i<this.invoicePallets.length;i++){ 
      var weight = this.invoicePallets[i].Weight;   
      this.TotalUsd = weight * this.invoicePallets[i].Pallet.Price + this.TotalUsd;
    }

    for(var i=0;i<this.invoiceBoxes.length;i++){ 
      var weight = this.invoiceBoxes[i].Weight;   
      this.TotalUsd = weight * this.invoiceBoxes[i].Box.Price + this.TotalUsd;
    }

    this.TotalUsd = this.TotalUsd - this.invoice.Discount;

    this.TotalUsd = Math.round(this.TotalUsd * 1000)/1000;
    this.Tax = this.TotalUsd * this.invoice.Kdv /100;
    this.Tax = Math.round(this.Tax * 1000)/1000;
    this.TotalUsd = this.TotalUsd + this.Tax;
    this.TotalLira = this.TotalUsd  * this.ExchangeRate;
    this.TotalLira = Math.round(this.TotalLira * 1000)/1000;    

  }

  Save(invoice: Invoice) {
  this.Calculate();
    this.showSuccess = false;
    var invoiceURL = "/api/Invoice/Update";
    this.invoice.InvoicePallets = new Array<InvoicePallet>();
    for(var i=0;i<this.invoicePallets.length;i++){
      var invoicePallet = new InvoicePallet();
      invoicePallet.InvoiceId = this.invoice.InvoiceId;
      invoicePallet.PalletId = this.invoicePallets[i].PalletId;
      invoicePallet.Weight = this.invoicePallets[i].Weight;
      invoice.InvoicePallets.push(invoicePallet);
    }

    this.invoice.InvoiceBoxes = new Array<InvoiceBox>();
    for(var i=0;i<this.invoiceBoxes.length;i++){
      var invoiceBox = new InvoiceBox();
      invoiceBox.InvoiceId = this.invoice.InvoiceId;
      invoiceBox.BoxId = this.invoiceBoxes[i].BoxId;
      invoiceBox.Weight = this.invoiceBoxes[i].Weight;
      invoice.InvoiceBoxes.push(invoiceBox);
    }        
    invoice.ExchangeRate = this.ExchangeRate;
    invoice.TotalUsd = this.TotalUsd;
    invoice.Tax = this.Tax;
    
    this.http
      .post<Invoice>(invoiceURL, invoice)
      .toPromise()
      .then(res => {
        this.showSuccess = true;
      });
  }

  UpdateCustomer(option:Customer){
    this.invoice.Customer = option;
  }
  Cancel() {
    this.router.navigateByUrl("/home");
  }
}
