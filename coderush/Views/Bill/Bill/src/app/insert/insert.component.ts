import { Component, OnInit } from '@angular/core';
import { Vendor } from '../../../../../Vendor/Vendors/src/app/Vendor';
import { Bill } from '../bill';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-insert',
  templateUrl: './insert.component.html',
  styleUrls: ['./insert.component.sass']
})
export class InsertComponent implements OnInit {
  bill = new Bill();
  
  showSuccess = false;
  showError = false;
  myControl = new FormControl();
  VendorsControl = new FormControl();
  vendors = new Array<Vendor>();
  filteredOptions: Observable<Vendor[]>;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.bill.Vendor = new Vendor();
    this.bill.Amount =0;
    this.bill.BillDueDate = new Date('9/9/99');
    this.bill.Paid=0;
    this.bill.KDV =0;    
  }

  ngOnInit() {

    var vendorUrl = "/api/Vendor/";    

    this.http.get<any>(vendorUrl)
    .subscribe(p => {
      this.vendors = p.Items;
      this.filteredOptions = this.VendorsControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    });

   
  }

  Save(bill: Bill) {
    this.showSuccess = false;
    var billUrl = "/api/Bill/Insert";
    
    this.http
      .post<Bill>(billUrl, bill)
      .toPromise()
      .then(res => {
        this.showSuccess = true;
      });
  }

  UpdateVendor(option:Vendor){
    this.bill.Vendor = option;
  }
  
  Cancel() {
    this.router.navigateByUrl("/home");
  }

  private _filter(value: string): Vendor[] {
    const filterValue = value.toString().toLowerCase();

    return this.vendors.filter(option => option.VendorName.toLowerCase().includes(filterValue));
  }
}
