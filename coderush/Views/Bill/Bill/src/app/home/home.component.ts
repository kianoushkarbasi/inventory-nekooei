import { Component, OnInit } from "@angular/core";
import { Bill } from "../bill";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  bills = new Array<Bill>();
  preventSingleClick = false;
  timer: any;
  delay: Number;
  searchText: string;

  constructor(private http: HttpClient, private router: Router) {}

  ngOnInit() {
    var billURL = "/api/Bill/";
    this.http
      .get<Bill[]>(billURL)
      .toPromise()
      .then(res => {
        this.bills = res["Items"];
      });
  }

  doubleClick(event) {
    this.preventSingleClick = true;
    clearTimeout(this.timer);
    //Navigate on double click
    this.router.navigate(["/edit/"], {
      queryParams: { id: event.currentTarget.children[0].innerText }
    });
  }
}
