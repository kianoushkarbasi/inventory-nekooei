import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { Bill } from "../bill";
import { FormControl } from "@angular/forms";
import { Vendor } from "../../../../../Vendor/Vendors/src/app/Vendor";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  bill = new Bill();
  showSuccess = false;
  showError = false;
  myControl = new FormControl();    
  vendors = new Array<Vendor>();    
  KDV:number;
  ExchangeRate:number;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.bill.Vendor = new Vendor();        
  }

  ngOnInit() {

    var vendorUrl = "/api/Vendor/";    
    var billUrl = "/api/Bill/GetBill/";
    
    var id = this.route.snapshot.queryParamMap.get("id");

    this.http.get<any>(vendorUrl)
    .subscribe(p => {
      this.vendors = p.Items;
    });


    this.http.get<any>(billUrl +  id)
    .subscribe(p => {
      this.bill = p;
    });

  }

  Delete(){        
    this.showSuccess = false;
    var billUrl = "/api/Bill/Delete/?billId=" + this.bill.BillId;
    this.http
      .delete(billUrl)
      .toPromise()
      .then(res => {
        this.showSuccess = true;
        this.router.navigateByUrl("/home");
      });
  }

  Save(bill: Bill) {
  
    this.showSuccess = false;
    var billURL = "/api/Bill/Update";
   
    this.http
      .post<Bill>(billURL, bill)
      .toPromise()
      .then(res => {
        this.showSuccess = true;
      });
  }

  UpdateVendor(option:Vendor){
    this.bill.Vendor = option;
  }
  Cancel() {
    this.router.navigateByUrl("/home");
  }
}
