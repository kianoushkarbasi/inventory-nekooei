export class Customer {
  public CustomerId: number;
  public CustomerName: String;
  public Address: String;
  public City: String;
  public ZipCode: String;
  public Phone: String;
  public MobilePhone: String;
  public Email: String;
  public Website: String;
  public Vergi: String;
}
