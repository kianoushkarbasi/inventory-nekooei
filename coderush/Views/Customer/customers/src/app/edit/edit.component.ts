import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { Customer } from "../Customer";

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.sass"]
})
export class EditComponent implements OnInit {
  customer = new Customer();
  showSuccess = false;
  showError = false;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) {}
  ngOnInit() {
    var id = this.route.snapshot.queryParamMap.get("id");
    var customerURL = "/api/Customer/GetCustomer/" + id;
    this.http
      .get<Customer>(customerURL)
      .toPromise()
      .then(res => {
        this.customer = res;
      });
  }

  Save(customer: Customer) {
    this.showSuccess = false;
    var customerURL = "/api/Customer/Update";
    this.http
      .put<Customer>(customerURL, customer)
      .toPromise()
      .then(res => {
        this.showSuccess = true;
      });
  }

  Delete(customer: Customer) {
    this.showSuccess = false;
    var customerURL = "/api/Customer/Remove/" + customer.CustomerId;
    this.http
      .delete<Customer>(customerURL)
      .toPromise()
      .then(res => {
        this.showSuccess = true;
        this.router.navigateByUrl("/index");
      });
  }
  Cancel(customer: Customer) {
    this.router.navigateByUrl("/index");
  }
}
