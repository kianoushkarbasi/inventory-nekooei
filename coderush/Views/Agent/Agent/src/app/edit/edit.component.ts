import { Component, OnInit } from '@angular/core';
import { Agent } from '../Agent';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Customer } from '../../../../../Customer/customers/src/app/Customer';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {
  agent = new Agent();
  
  showSuccess = false;
  showError = false;
  myControl = new FormControl();
  CustomersControl = new FormControl();
  customers = new Array<Customer>();
  filteredOptions: Observable<Customer[]>;
  newAgentCustomer:number;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.agent.Customers = new Array<Customer>();
  }

  ngOnInit() {

    var id = this.route.snapshot.queryParamMap.get("id");

    var customerUrl = "/api/Customer/";  
    var agentUrl = "/api/Agent/GetAgent/"
    
    this.http.get<any>(customerUrl)
    .subscribe(p => {
      this.customers = p.Items;
      this.filteredOptions = this.CustomersControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    });

    this.http.get<any>(agentUrl +  id)
    .subscribe(p => {
      this.agent = p;
    });
   
  }

  Delete(){        
    this.showSuccess = false;
    var agentUrl = "/api/Agent/Remove/" + this.agent.AgentId;
    this.http
      .delete(agentUrl)
      .toPromise()
      .then(res => {
        this.showSuccess = true;
        this.router.navigateByUrl("/home");
      });
  }

  Save(agent: Agent) {
  
    this.showSuccess = false;
    var billURL = "/api/Agent/Update";
   
    this.http
      .put<Agent>(billURL, agent)
      .toPromise()
      .then(res => {
        this.showSuccess = true;
      });
  }

  UpdateCustomer(option:Customer){
    this.agent.Customers.push(option);
  }
  
  Cancel() {
    this.router.navigateByUrl("/home");
  }

  RemoveItem(customer:Customer){
    const index: number = this.agent.Customers.indexOf(customer);
    if (index !== -1) {
        this.agent.Customers.splice(index, 1);
    }           
  }

  private _filter(value: string): Customer[] {
    const filterValue = value.toString().toLowerCase();

    return this.customers.filter(option => option.CustomerName.toLowerCase().includes(filterValue));
  }
}
