﻿using System;
using System.ComponentModel.DataAnnotations;

namespace coderush.Models
{
    public class Bill
    {
        public int BillId { get; set; }
        [Display(Name = "Bill / Invoice Number")]
        public string BillName { get; set; }
        [Display(Name = "Vendor Bill / Invoice #")]
        public string VendorInvoiceNumber { get; set; }
        [Display(Name = "Bill Date")]
        public DateTimeOffset BillDate { get; set; }
        [Display(Name = "Bill Due Date")]
        public DateTimeOffset BillDueDate { get; set; }        
        public double Amount{get;set;}
        public double UsdAmount{get;set;}
        public double Paid{get;set;}
        public double KDV{get;set;}
        public Vendor Vendor{get;set;}
        public double ExchngeRate{get;set;}
        public string Comment{get;set;}
    }
}
