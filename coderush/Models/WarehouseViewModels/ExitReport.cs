namespace coderush.Models.WarehouseViewModels
{
    public class ExitReport
    {
        public string ExitDate{get;set;}
        public string Name{get;set;}      
        public string Color{get;set;}
        public string ColorCode{get;set;}
        public string Denier{get;set;}
        public string PalletBarcode{get;set;}
        public string PalletWeight{get;set;}
        public string YarnType{get;set;}
    }
}
