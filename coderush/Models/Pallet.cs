﻿using System.Collections.Generic;

namespace coderush.Models
{
    public class Pallet
    {
        public Pallet()
        {
        }
        public int PalletId {get;set;}
        public string PalletNumber{get;set;}
        public string Barcode{get;set;}
        public string YarnType{get;set;}
        public int Denier{get;set;}
        public int Filament{get;set;}
        public string Intermingle{get;set;}
        public string Color{get;set;}
        public string ColorCode{get;set;}
        public string BobbinSize{get;set;}
        public string PalletName{get;set;}
        public double Weight{get;set;}
        public double Price{get;set;}
        public double RemainWeight{get;set;}
        public bool Sold{get;set;}        
        public string Note{get;set;}
        public IList<InvoicePallet> InvoicePallets{get;set;}
    }
}
