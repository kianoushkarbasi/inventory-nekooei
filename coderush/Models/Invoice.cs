﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace coderush.Models
{
    public class Invoice
    {
        public Invoice(){   
            InvoicePallets = new List<InvoicePallet>();
            InvoiceBoxes = new List<InvoiceBox>();

        }
        [Display(Name = "Invoice Number")]
        public int InvoiceId { get; set; }
                
        [Display(Name = "Invoice Date")]
        public DateTimeOffset InvoiceDate { get; set; }
        [Display(Name = "Invoice Due Date")]
        public DateTimeOffset InvoiceDueDate { get; set; }

        [Display(Name = "Customer")]
        public Customer Customer{get;set;}       

        [Display(Name = "TotalUsd")]
        public double TotalUsd{get;set;}

        [Display(Name = "Tax")]
        public double Tax{get;set;}
        public double Kdv{get;set;}
        [Display(Name = "Exchange Rate")]
        public double ExchangeRate{get;set;}
        public int StateInvoiceNumber{get;set;}        
        public double Discount{get;set;}
        public double Paid{get;set;}
        public string Currency {get;set;}
        public IList<InvoicePallet> InvoicePallets{get;set;}
        public IList<Invoice‌Box> InvoiceBoxes{get;set;}
    }
}
