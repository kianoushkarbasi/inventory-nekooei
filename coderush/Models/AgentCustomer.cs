
namespace coderush.Models
{
    public class AgentCustomer
    {
        public int CustomerId{get;set;}
        public int AgentId { get; set; }        

        public Agent Agent{get;set;}
        public Customer Customer{get;set;}
    }
}
